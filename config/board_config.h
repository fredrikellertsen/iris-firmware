#ifndef BOARDCONFIG_H
#define BOARDCONFIG_H

// === BOARD TYPE CONFIG ===
//#define BREADBOARD
#define REV1

// === MISC DEFINITIONS ===
#define USES_DFU					true	// true if DFU is enabled
#define FW_VERSION					0x06

// === HARDWARE CONFIG ===
#define HW_N_LEDS					8
#define HW_LED_CURRENT_DEFAULT		50		// 5 mA
#define HW_LED_CURRENT_MAX			100		// 10 mA
#define HW_MCU_VERSION				0x02	// nRF52
#define HW_ACCELEROMETER_VERSION	0x02	// ICM20648
#define HW_OS_VERSION				0x01	// BPW34S
#define HW_CRITICAL_TEMPERATURE		100

// === STM PUSHBUTTON PIN DEFINITIONS ===
#define PIN_STM_HOLD				21
#define PIN_STM_PBO					20
#define PIN_STM_RST					19

// === BATTERY CHARGER PIN DEFINITIONS ===
#define PIN_BAT_CHG					10
#define PIN_BAT_PGOOD				9

// === INDICATOR LED PIN DEFINITIONS ===
#define PIN_INDICATOR_RED			29
#define PIN_INDICATOR_BLUE			30
#define PIN_INDICATOR_GREEN			31

// === ADC REV1 (ADS1292) PIN DEFINITIONS ===
#define PIN_ADS1292_RESET			18
#define PIN_ADS1292_START			17
#define PIN_ADS1292_CS				16
#define PIN_ADS1292_MOSI			15
#define PIN_ADS1292_SCLK			14   
#define PIN_ADS1292_MISO			13
#define PIN_ADS1292_DRDY			12

// === SENSE PIN DEFINITIONS ===
#define PIN_BATSENSE				NRF_SAADC_INPUT_AIN3 // maps to pin 5
#define PIN_THERMISTOR				82

#ifdef REV1
	// === LED DRIVER REV1 (LP55231) PIN DEFINITIONS ===
	#define PIN_LP55231_SDA			6
	#define PIN_LP55231_SCL			7
	#define PIN_LP55231_EN			22
	#define PIN_LP55231_CLK			26
	#define PIN_LP55231_INT			23
#endif

#ifdef BREADBOARD
	// === LED DRIVER BREADBOARD (LP55231) PIN DEFINITIONS ===
	#define PIN_LP55231_SDA			28
	#define PIN_LP55231_SCL			4
	#define PIN_LP55231_EN			3
	#define PIN_LP55231_CLK			26
	#define PIN_LP55231_INT			23
#endif

// === INTERFACE DEFINITIONS ===
#define ADS1292_SPI_INSTANCE		1
#define LP55231_TWI_INSTANCE		0
#define ICM20648_TWI_INSTANCE		1

#endif // !BOARDCONFIG_H
