#ifndef BIOSENSE_CALIBRATE_H_
#define BIOSENSE_CALIBRATE_H_

#include "lp55231.h"
#include "ads1292.h"
#include "nrf_delay.h"
#include "nrf_log.h"
#include "math.h"

#define CALIBRATION_RANGE_TARGET	(0.5 * pow(2, 24)) * 0.9
#define CALIBRATION_TARGET			0

/*******************************************************************************************
 * @brief: Calibrates the current for all LED groups in order to place biosense samples between 
 * ±CALIBRATION_RANGE_TARGET and as close to CALIBRATION_TARGET as possible. If it is unable
 * to do so, it will restore all LED currents to their defaults. This function is not blocking,
 * so the user must refrain from sending commands to LP55231 and ADS1292 while this is running.
 * @param[in]	m_LED_groups				Pointer to the LED groups to calibrate
 * @param[in]	number_of_LED_groups		The number of LED groups in m_LED_groups
 * @param[in]	calibration_finished_cb		Callback for finished calibrations. Called with
 *											true if calibration was successful.
 *******************************************************************************************/
void calibrate_LED_group_currents(const led_group_t* m_LED_groups, uint8_t number_of_LED_groups, void(*calibration_finished_cb)(bool));

#endif // !BIOSENSE_CALIBRATE_H_