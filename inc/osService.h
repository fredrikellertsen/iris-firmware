#ifndef OS_SERVICE_H_
#define OS_SERVICE_H_

// Standard includes
#include <stdint.h>
#include <stdbool.h>

#include "bleSupport.h"

/*******************************************************************************************
 * @brief: Initializes the optical sensor service
 * @param[in] m_os_service Pointer to the optical sensor service struct
 *******************************************************************************************/
void ble_os_service_init(ble_os_service_t* m_os_service);

/*******************************************************************************************
 * @brief: Function for handling ble events relevant to the optical sensor service
 * @param[in] m_os_service Pointer to the optical sensor service
 * @param[in] p_ble_evt  Event received from the BLE stack
 *******************************************************************************************/
void ble_os_service_on_ble_evt(ble_os_service_t* m_os_service, ble_evt_t* p_ble_evt);

/*******************************************************************************************
 * @brief: Puts data onto optical sensor samples characteristic and notifies client (if 
 * client is connected AND subscribed to this characteristic)
 * @param[in] m_os_service Pointer to the optical sensor service
 * @param[in] data The data to be notified
 *******************************************************************************************/
void ble_os_service_notify(ble_os_service_t* m_os_service, uint8_t* data);

#endif // !OS_SERVICE_H_