#ifndef LED_HANDLER_H
#define LED_HANDLER_H

// Standard includes
#include <stdint.h>
#include <stdbool.h>

#include "bleSupport.h"
#include "biosenseHandler.h"

/*******************************************************************************************
 * @brief: Initializes the LEDs service
 * @param[in]	m_leds_service	Pointer to the LEDs serice struct
 *******************************************************************************************/
void ble_leds_service_init(ble_leds_service_t* m_leds_service);

/*******************************************************************************************
 * @brief: Function for dispatching BLE Stack events related to LEDs service and 
 * LED characteristics. Any LP55231-related events go through biosenseHandler
 * @param[in]	m_leds_service	Pointer to the LEDs serice struct
 * @param[in]	p_ble_evt		Event received from the BLE stack. 
 *******************************************************************************************/
void ble_leds_service_on_ble_evt(ble_leds_service_t* m_leds_service, ble_evt_t* p_ble_evt);

#endif // !LED_HANDLER_H