#ifndef ADS1292_H_
#define ADS1292_H_

// modes in which the ADC can run
typedef enum {
	TEST = 0,		//< generate internal test signal
	SINGLE_SHOT,	//< upon ads1292_start_sample, one sample is taken. see datasheet p. 33
	CONTINUOUS		//< upon ads1292_start_sample, continuous sampling is enabled. call ads1292_stop_sample to stop.
}ads1292_modes;

/*******************************************************************************************
 * @brief: Initializes SPI interface with ADS1292 module and GPIO needed for ADS1292 control.
 * This function must be called before any other function calls in this module.
 *******************************************************************************************/
void ads1292_init();

/*******************************************************************************************
 * @brief: Set ADC sample rate.
 * @param[in]	rate	Byte corersponding to the rate to set (see defines for allowed rates)
 *******************************************************************************************/
void ads1292_set_sample_rate(uint8_t rate);

/*******************************************************************************************
 * @brief: Set ADS PGA gain setting. 
 * @param[in]	gain	The gain to set. Valid gains: 1, 2, 3, 4, 6, 8, 12
 *******************************************************************************************/
void ads1292_set_pga_gain(uint8_t gain);

/*******************************************************************************************
 * @brief: Performs offset calibration as per p. 36 in the datasheet.
 *******************************************************************************************/
void ads1292_perform_offset_calibration(void);

/*******************************************************************************************
 * @brief: Starts a sample conversion by pulling START high. The effect of this depends
 * on the current ADS1292 mode. Before starting conversions, the ADC is set in RDATAC mode
 * for fast data retrieval. Data ready is checked via GPIOTE pin change interrupts
 * @param[in]	data_ready_cb	Callback function for when data is ready, see drdy_cb() for 
 * "data ready" implementation
 * @param[in]	mode	The mode to start the ADC in (see ads1292_modes).
 *******************************************************************************************/
void ads1292_start(void(*data_ready_cb)(void), ads1292_modes mode);

/*******************************************************************************************
 * @brief: Stops further ADS1292 samples by pulling START low and, if in RDATAC mode, will
 * send the SDATAC command.
 *******************************************************************************************/
void ads1292_stop(void);

/*******************************************************************************************
 * @brief: Reads sample data from a single channel
 * @param[in]	dataPtr		Pointer to byte array in which to store data
 * @param[in]	channel		The channel to read
 *******************************************************************************************/
void ads1292_read_sample(uint8_t* dataPtr, uint8_t channel);

// Register map
#define ADSREG_ID			0x00
#define ADSREG_CONFIG1		0x01
#define ADSREG_CONFIG2		0x02
#define ADSREG_LOFF			0x03
#define ADSREG_CH1SET		0x04
#define ADSREG_CH2SET		0x05
#define ADSREG_RLD_SENS		0x06
#define ADSREG_LOFF_SENS	0x07
#define ADSREG_LOFF_STAT	0x08
#define ADSREG_RESP1		0x09
#define ADSREG_RESP2		0x0A
#define ADSREG_GPIO			0x0B

// System commands
#define ADSCMD_WAKEUP		0x02
#define ADSCMD_STANDBY		0x04
#define ADSCMD_RESET		0x06
#define ADSCMD_START		0x08
#define ADSCMD_STOP 		0x0A
#define ADSCMD_OFFSETCAL	0x1A

// Data read commands
#define ADSCMD_RREG			0x20
#define ADSCMD_WREG			0x40

// Register commands
#define ADSCMD_RDATAC		0x10
#define ADSCMD_SDATAC		0x11
#define ADSCMD_RDATA		0x12

// Allowed sample rates
#define ADS_SAMPLE_RATE_125	0b000
#define ADS_SAMPLE_RATE_250	0b001
#define ADS_SAMPLE_RATE_500	0b010
#define ADS_SAMPLE_RATE_1000	0b011
#define ADS_SAMPLE_RATE_2000	0b100
#define ADS_SAMPLE_RATE_4000	0b101
#define ADS_SAMPLE_RATE_8000	0b110

#endif 