#ifndef DEVICE_INFO_SERVICE_H
#define DEVICE_INFO_SERVICE_H

#include "bleSupport.h"
#include "board_config.h"

/*******************************************************************************************
 * @brief: Initializes the device info service and all its characteristics (read only for all)
 * @param[in]	m_device_info_service	Pointer to the device info service struct
 *******************************************************************************************/
void ble_device_info_srv_init(ble_device_info_service_t* m_device_info_service);

#endif //!DEVICE_INFO_SERVICE_H