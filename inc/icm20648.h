#ifndef ICM_20648_H_
#define ICM_20648_H_

/*	IMPORTANT
*
*	TWI0 and TWI1 cannot be enabled at the same time (in sdk_config)
*	Future IRIS revs will have LP55231 and ICM20648 on the same I2C (TWI) bus
*	which fixes this problem.
*
*	This module is untested and WILL contain bugs, beware
*/

// std includes
#include <stdbool.h>
#include <stdint.h>

// SDK includes
#include "app_error.h"
#include "app_util_platform.h"
#include "nrf_drv_twi.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_gpio.h"
#include "nrf_delay.h"

// Project inlcudes
#include "board_config.h"

void icm20648_init(void);
void icm20648_reset(void);

// ICM20648 I2C address
#define ICM20648_ADDRESS 0b1101001

// Register map
#define ICM20648_REG_WHO_AM_I		0x00
#define ICM20648_REG_USER_CTRL		0x03



#endif //!ICM_20648_H_