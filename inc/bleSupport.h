#ifndef BLE_SUPPORT_C_
#define BLE_SUPPORT_C_

// Standard includes
#include <stdbool.h>
#include <stdint.h>
#include <string.h>

// SDK12 includes
#include "app_error.h"
#include "app_timer.h"
#include "ble_advdata.h"
#include "ble_advertising.h"
#include "ble_conn_params.h"
#include "ble_conn_state.h"
#include "ble_hci.h"
#include "ble_srv_common.h"
#include "boards.h"
#include "fds.h"
#include "fstorage.h"
#include "nordic_common.h"
#include "nrf.h"
#include "nrf_delay.h"
#include "nrf_error.h"
#include "nrf_gpio.h"
#include "peer_manager.h"
#include "sensorsim.h"
#include "softdevice_handler.h"

#include "ble_gatts.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"

#define IS_SRVC_CHANGED_CHARACT_PRESENT 1                                           /**< Include or not the service_changed characteristic. if not enabled, the server's database cannot be changed for the lifetime of the device*/

#define NRF_BLE_PREFERRED_MTU_SIZE		105											/**< MTU size used in the softdevice enabling and to reply to a BLE_GATTS_EVT_EXCHANGE_MTU_REQUEST event. */

#define APP_FEATURE_NOT_SUPPORTED       BLE_GATT_STATUS_ATTERR_APP_BEGIN + 2        /**< Reply when unsupported features are requested. */

#define CENTRAL_LINK_COUNT              0                                           /**< Number of central links used by the application. When changing this number remember to adjust the RAM settings*/
#define PERIPHERAL_LINK_COUNT           1                                           /**< Number of peripheral links used by the application. When changing this number remember to adjust the RAM settings*/

#define DEVICE_NAME                     "IRIS"			                            /**< Name of device. Will be included in the advertising data. */
#define MANUFACTURER_NAME               "MOON Wearables"							/**< Manufacturer. Will be passed to Device Information Service. */
#define APPEARANCE						BLE_APPEARANCE_PULSE_OXIMETER_WRIST_WORN	/**< Appearance of the device. */
#define APP_ADV_INTERVAL                300                                         /**< The advertising interval (in units of 0.625 ms. This value corresponds to 187.5 ms). */
#define APP_ADV_TIMEOUT_IN_SECONDS      180                                         /**< The advertising timeout in units of seconds. */
	
#define MIN_CONN_INTERVAL               MSEC_TO_UNITS(7.5, UNIT_1_25_MS)            /**< Minimum acceptable connection interval. */
#define MAX_CONN_INTERVAL               MSEC_TO_UNITS(30, UNIT_1_25_MS)             /**< Maximum acceptable connection interval. */
#define SLAVE_LATENCY                   0                                           /**< Slave latency. */
#define CONN_SUP_TIMEOUT                MSEC_TO_UNITS(4000, UNIT_10_MS)             /**< Connection supervisory timeout (4 seconds). */

#define FIRST_CONN_PARAMS_UPDATE_DELAY  APP_TIMER_TICKS(5000, APP_TIMER_PRESCALER)  /**< Time from initiating event (connect or start of notification) to first time sd_ble_gap_conn_param_update is called (5 seconds). */
#define NEXT_CONN_PARAMS_UPDATE_DELAY   APP_TIMER_TICKS(30000, APP_TIMER_PRESCALER) /**< Time between each call to sd_ble_gap_conn_param_update after the first call (30 seconds). */
#define MAX_CONN_PARAMS_UPDATE_COUNT    3                                           /**< Number of attempts before giving up the connection parameter negotiation. */

#define SEC_PARAM_BOND                  1                                           /**< Perform bonding. */
#define SEC_PARAM_MITM                  0                                           /**< Man In The Middle protection not required. */
#define SEC_PARAM_LESC                  0                                           /**< LE Secure Connections not enabled. */
#define SEC_PARAM_KEYPRESS              0                                           /**< Keypress notifications not enabled. */
#define SEC_PARAM_IO_CAPABILITIES       BLE_GAP_IO_CAPS_NONE                        /**< No I/O capabilities. */
#define SEC_PARAM_OOB                   0                                           /**< Out Of Band data not available. */
#define SEC_PARAM_MIN_KEY_SIZE          7                                           /**< Minimum encryption key size. */
#define SEC_PARAM_MAX_KEY_SIZE          16                                          /**< Maximum encryption key size. */

// Struct containing characteristic properties
typedef struct {
	bool read, write, notify;	// Booleans defining characteristic properties
	uint16_t length;			// Length of the characteristic value in bytes
	uint8_t* init_value;		// The initial value of the characteristic. If NULL, value is zero.
}ble_char_props_t;

// General struct for BLE characteristics which can support read, write and notify.
typedef struct {
	ble_gatts_char_handles_t	char_handle;	// Handles of the characteristic attributes
	ble_uuid_t					char_uuid;		// UUID (16 bit) of the characteristic
	ble_char_props_t			char_props;		// Characteristic properties 
}ble_char_t;

// Struct for the leds service
typedef struct {
	uint16_t	conn_handle;			
	uint16_t	service_handle;			
	ble_char_t	led_ctrl_pt_char;
	ble_char_t	led_group_ctrl_pt_char;
}ble_leds_service_t;

// Struct for the optical sensor service
typedef struct {
	uint16_t	conn_handle;			
	uint16_t	service_handle;			
	ble_char_t	ctrl_pt_char;
	ble_char_t	samples_char;
	bool		is_client_subscribed;
}ble_os_service_t;

// Struct for the system service
typedef struct {
	uint16_t	conn_handle;			
	uint16_t	service_handle;			
	ble_char_t	ctrl_pt_char;
	ble_char_t	ctrl_pt_resp_char;
	ble_char_t	err_msg_char;
	ble_char_t	id_char;
	bool		is_client_subscribed_to_errors;
	bool		is_client_subscribed_to_ctrl_pt;
}ble_system_service_t;

// Struct for the system service
typedef struct {
	uint16_t	conn_handle;			
	uint16_t	service_handle;			
	ble_char_t	name_char;
	ble_char_t	fw_version_char;
	ble_char_t	manufacturer_char;
	ble_char_t	mcu_version_char;
	ble_char_t	acc_version_char;
	ble_char_t	os_version_char;
	ble_char_t	device_identifier_char;
}ble_device_info_service_t;

// Struct for the battery level service
typedef struct {
	uint16_t	conn_handle;			
	uint16_t	service_handle;			
	ble_char_t	battery_level_char;
	bool		is_client_subscribed;
}ble_bat_service_t;

// Struct for the temperature service
typedef struct {
	uint16_t	conn_handle;			
	uint16_t	service_handle;			
	ble_char_t	temperature_ctrl_point;
	ble_char_t	die_temperature_char;
	//ble_char_t	ntc_temperature_char; not implemented
	bool		is_client_subscribed_to_die_temp;
	//bool		is_client_subscribed_to_ntc_temp; not implemented
}ble_temperature_service_t;

/*******************************************************************************************
 * @brief: Adds a characteristic to a service
 * @param[in]	service_handle	Service handle of the enveloping service (as given to us by the 
 * softdevice)
 * @param[in]	ch	Pointer to the characteristic struct
 *******************************************************************************************/
void ble_add_characteristic(uint16_t service_handle, ble_char_t* ch);

void ble_log_hvx_err_code(uint32_t err_code);

// Base UUID for all IRIS services and characteristics
#define BLE_UUID_MTP_BASE {{0xA4, 0xF9, 0xC3, 0x93, 0x13, 0x11, 0x3E, 0x89, 0xC3, 0x47, 0xD1, 0xD7, 0x00, 0x00, 0x96, 0x7C}}

// === 16-bit SYSTEM UUIDs ===
#define BLE_UUID_SYSTEM_SRVC							0x0100
#define BLE_UUID_SYSTEM_CTRL_PT							0x0101
#define BLE_UUID_SYSTEM_CTRL_PT_RESP					0x0102
#define BLE_UUID_SYSTEM_ERR								0x0103

// === 16-bit DEVICE INFO UUIDs ===
#define BLE_UUID_DEVICE_INFO_SRVC						0x0200
#define BLE_UUID_DEVICE_INFO_NAME						0x0201
#define BLE_UUID_DEVICE_INFO_FW_VERSION					0x0202
#define BLE_UUID_DEVICE_INFO_MANUFACTURER				0x0203
#define BLE_UUID_DEVICE_INFO_MCU_VERSION				0x0204
#define BLE_UUID_DEVICE_INFO_ACCELEROMETER_VERSION		0x0205
#define BLE_UUID_DEVICE_INFO_OPTICAL_SENSOR_VERSION		0x0206
#define BLE_UUID_DEVICE_INFO_IDENTIFIER					0x0207

// === 16-bit BATTERY UUIDs (defined by SIG) ===
#define BLE_UUID_BATTERY_SRVC							0x180F
#define BLE_UUID_BATTERY_LEVEL							0x2A19

// === 16-bit LED UUIDs ===
#define BLE_UUID_LED_SRVC								0x0400
#define BLE_UUID_LED_CTRL_PT							0x0401
#define BLE_UUID_LED_GROUP_CTRL_PT						0x0402

// === 16-bit OPTICAL SENSOR UUIDs ===
#define BLE_UUID_OPTICAL_SENSOR_SRVC					0x0500
#define BLE_UUID_OPTICAL_SENSOR_CTRL_PT					0x0501
#define BLE_UUID_OPTICAL_SENSOR_SAMPLES					0x0502

// === 16-bit ACCELEROMETER UUIDs ===
#define BLE_UUID_ACCELEROMETER_SRVC						0x0600
#define BLE_UUID_ACCELEROMETER_CTRLPT					0x0601
#define BLE_UUID_ACCELEROMETER_ACCELERATION				0x0602

// === 16-bit TEMPERATURE UUIDs ===
#define BLE_UUID_TEMPERATURE_SERVICE					0x0700
#define BLE_UUID_TEMPERATURE_CTRLPT						0x0701
#define BLE_UUID_TEMPERATURE_NTC_TEMPERATURE			0x0702
#define BLE_UUID_TEMPERATURE_DIE_TEMPERATURE			0x0703

// === 16-bit TESTING UUIDs ===
#define BLE_UUID_TESTING_SRVC							0x0800
#define BLE_UUID_TESTING_CTRLPT							0x0801
#define BLE_UUID_TESTING_RAMPUP							0x0802
#define BLE_UUID_TESTING_ROCKETMODE						0x0803

#endif // !BLE_SUPPORT_C_