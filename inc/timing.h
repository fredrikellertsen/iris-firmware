#ifndef TIMING_H
#define TIMING_H

// SKD Includes
#include "app_timer.h"
#include "nrf_log.h"

#define APP_TIMER_PRESCALER				0	/**< Value of the RTC1 PRESCALER register. */
#define APP_TIMER_OP_QUEUE_SIZE			4	/**< Size of timer operation queues. */

APP_TIMER_DEF(temperature_timer_id);
APP_TIMER_DEF(biosense_timer_id);
APP_TIMER_DEF(battery_measurement_timer_id);

/******************************************************************************************
 * @brief: Creates a repeated timer from an id and gives it a timeout handler
 * @param{in] id				Timer ID, created by APP_TIMER_DEF()
 * @param{in] timeout_handler	Timeout handler function for timeout events
 ******************************************************************************************/
void timer_init(app_timer_id_t id, app_timer_timeout_handler_t timeout_handler);

/******************************************************************************************
 * @brief: Starts a timer with timeout interval
 * @param{in] id					Timer ID, created by APP_TIMER_DEF()
 * @param{in] timeout_interval_ms	Timeout interval in ms
 * @param{in] p_context				General purpose pointer, will be passed to the timeout 
 * handler on timeout
 ******************************************************************************************/
void timer_start(app_timer_id_t id, uint32_t timeout_interval_ms, void* p_context);

/******************************************************************************************
 * @brief: Stops timer
 * @param{in]	id	Timer ID, created by APP_TIMER_DEF()
 ******************************************************************************************/
void timer_stop(app_timer_id_t id);

#endif
