#ifndef BATTERY_H_
#define BATTERY_H_

#include "bleSupport.h"

/* This is a shitty module and I can't be bothered to document it. Should
 * be completely rewritten using the nRF52 SDK battery service and proper 
 * ADC control */

void battery_measurement_init(void);
void battery_measurement_timer_timeout_handler(void * p_context);
void ble_battery_measurement_on_ble_evt(ble_evt_t* p_ble_evt);

#endif //!BATTERY_H_