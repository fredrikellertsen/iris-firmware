#ifndef BIOSENSE_HANDLER_H
#define BIOSENSE_HANDLER_H

// Standard library includes
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

// SDK includes
#include "nrf_gpio.h"
#include "ble_srv_common.h"
#include "app_error.h"
#include "nrf_error.h"

// Project includes
#include "ads1292.h"
#include "lp55231.h"
#include "timing.h"
#include "bleSupport.h"
#include "systemService.h"
#include "biosenseCalibrate.h"

#define BIOSENSE_LOOP_PERIOD_DEFAULT	5	//< default period (ms) of biosense loops
#define BIOSESE_DATA_TARGET_SIZE		99	//< transmit after collecting this many bytes of data

#if (NRF_BLE_PREFERRED_MTU_SIZE - BIOSESE_DATA_TARGET_SIZE) < 3
#error "Current MTU size does not permit this data length"
#endif

/*******************************************************************************************
 * @brief: Initializes biosense
 * @param[in]	txrdy_cb	Callback for when x number of loops have finished and 
 * BIOSENSE_DATA_TARGET_SIZE bytes have been gathered.
  *******************************************************************************************/
void biosense_init(void(*txrdy_cb)(uint8_t*));

/*******************************************************************************************
 * @brief: Starts a biosense loop. 
  *******************************************************************************************/
void biosense_start(void);

/*******************************************************************************************
 * @brief: Stops the biosense loop. If biosense was running in auto increment mode, all LEDs
 * will also be cleared
  *******************************************************************************************/
void biosense_stop(void);

/*******************************************************************************************
 * @brief: Sets LED group parameters. LED group PWM is only allowed when biosense is not 
 * not running or running without auto increment
 * @param[in]	data	Byte array defining parameter and value (see MTP)
 * @param[in]	length	Byte array length
  *******************************************************************************************/
void biosense_set_led_group_param(uint8_t* data, uint8_t length);

/*******************************************************************************************
 * @brief: Sets parameters for a single LED. PWM is only allowed when biosense is not 
 * not running or running without auto increment
 * @param[in]	data	Byte array defining parameter and value (see MTP)
 * @param[in]	length	Byte array length
  *******************************************************************************************/
void biosense_set_led_param(uint8_t* data, uint8_t length);

/*******************************************************************************************
 * @brief: Sets parameters for the optical sensor (i.e. ADC).
 * @param[in]	data	Byte array defining parameter and value (see MTP)
 * @param[in]	length	Byte array length
  *******************************************************************************************/
void biosense_set_os_param(uint8_t* data, uint8_t length);

#endif // !BIOSENSE_HANDLER_H