#ifndef TEMPERATUREHANDLER_H
#define TEMPERATUREHANDLER_H

#include "bleSupport.h"

/*******************************************************************************************
 * @brief: Initializes the temperature service with characteristics (control point, die
 * temperature)
 *******************************************************************************************/
void ble_temperature_service_init(ble_temperature_service_t* m_temp_service);

/*******************************************************************************************
 * @brief: Function for handling ble events related to the temperature service
 * @param[in]	p_ble_evt	Event received from the BLE stack. 
 *******************************************************************************************/
void ble_temperature_on_ble_evt(ble_temperature_service_t* m_temp_service, ble_evt_t* p_ble_evt);

/*******************************************************************************************
 * @brief: Callback for temperature timer timeout. Will calculate temperature and notify 
 * client. If temperature is larger than 100 deg. C, system will power down.
 * @param[in]	p_context	General purpose pointer. It is assumed that this points to the
 * temperature service.
 *******************************************************************************************/
void temperature_timer_timeout_handler(void* p_context);

#endif