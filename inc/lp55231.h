#ifndef LED_DRIVER_H_
#define LED_DRIVER_H_

// std includes
#include <stdbool.h>
#include <stdint.h>

// Project inlcudes
#include "board_config.h"
#include "systemService.h"

#if HW_N_LEDS > 8
#error "HW_N_LEDS requires more than 8 bits in masks!"
#endif 

// Struct containing LED group information
typedef struct {
	uint8_t LED_mask;		//< Bit mask indicating LED members 
	uint16_t attack_us;		//< Time, in us, to delay before the LED group is assumed completely on
	uint16_t release_us;	//< Time, in us, to delay before the LED group is assumed completely off
}led_group_t;

/******************************************************************************************
 * @brief: Initializes the LP55231 module by enabling GPIO, TWI and setting LP55231 registers
 ******************************************************************************************/
void lp55231_init(void);

/******************************************************************************************
 * @brief:	Sets the current limit for LEDs
 * @param[in]	LED_mask	Bit mask defining LEDs (1 to 8). Subject to become an uint16 if 
 *							HW_N_LEDS changes.
 * @param[in]	current	The current in 100 uA steps (software limited to max 10 mA)
 ******************************************************************************************/
void lp55231_set_led_current(uint8_t LED_mask, uint8_t current);

/******************************************************************************************
 * @brief: Sets PWM frequency on LED(s).
 * @param[in]	LED_mask	Bit mask defining LEDs (1 to 8). Subject to become an uint16 if 
 *							HW_N_LEDS changes.
 * @param[in]	pwm_frequency The duty cycle to set (0-100)
 ******************************************************************************************/
void lp55231_set_led_pwm(uint8_t LED_mask, uint8_t pwm_value);

 /******************************************************************************************
 * @brief: Loads a program into execution engine 1
 * @param[in]	program_16b	The program to load
 * @param[in]	length		Length of the program to load
 ******************************************************************************************/
void lp55231_load_program(const uint16_t* program, uint16_t length);

 /******************************************************************************************
 * @brief: Runs program in excecution engine 1
 ******************************************************************************************/
void lp55231_run_program(void);

// LP55231 I2C address
#define LP55231_ADDRESS					0x32

// Page size of program memory
#define LP55231_PAGE_SIZE				16

// Register map (p. 21- http://www.ti.com/lit/ds/symlink/lp55231.pdf)
#define LP55231_REG_CNTRL1				0x00 // enable and engine execution
#define LP55231_REG_CNTRL2				0x01 // engine mode control
#define LP55231_REG_OUTPUT_LSB			0x05 // on/off control for D8-D1
#define LP55231_REG_LED_CTRL_BASE		0x06 // base register address for LED control	
#define LP55231_REG_LED_PWM_BASE		0x16 // base register address for PWM control	
#define LP55231_REG_LED_CURRENT_BASE	0x26 // base register address for LED current control	
#define LP55231_REG_MISC				0x36 // controls charge pump, clk source etc
#define LP55231_REG_ENG_PC_BASE			0x37 
#define LP55231_REG_ENG_STATUS			0x3A // engine status and interrupt
#define LP55231_REG_RESET				0x3D // writing 0xFF into this register resets LP55231
#define LP55231_REG_LED_TEST_CTRL		0x41
#define LP55231_REG_ENG_ENTRY_POINT		0x4C
#define LP55231_REG_PROGMEM_PAGESEL		0x4F
#define LP55231_REG_PROGMEM_START		0x50 // start page of program memory
#define LP55231_REG_ENG1_MAP_LSB		0x71

// Bit description in registers
#define LP55231_ENABLE					0x40
#define LP55231_ENG1_EXEC				0x20
#define LP55231_ENG2_EXEC				0x08
#define LP55231_ENG3_EXEC				0x02
#define LP55231_AUTO_INC				0x40
#define LP55231_PWR_SAVE				0x20
#define LP55231_PWM_PWR_SAVE			0x04
#define LP55231_CP_AUTO					0x18
#define LP55231_CP_OFF					0x00
#define LP55231_CP_1X					0x08
#define LP55231_CP_1X5					0x10
#define LP55231_EXT_CLK					0x02
#define LP55231_INT_CLK					0x1
#define LP55231_EN_LEDTEST				0x80
#define LP55231_LEDTEST_DONE			0x80
#define LP55231_RESET					0xFF
#define LP55231_ADC_SHORTCIRC_LIM		80
#define LP55231_EXT_CLK_USED			0x08
#define LP55231_ENG_STATUS_MASK			0x07
#define LP55231_FADER_MAPPING_MASK		0xC0
#define LP55231_FADER_MAPPING_SHIFT		6

static const uint16_t program_ramp_sequentially[] =
{
	0x9c10, // 0 map start (base 0x9c00)
	0x9c9d, // 1 map end (base 0x9c80)
	0x0260, // 2 ramp up	
	0x0200, // 3 wait
	0x0360, // 4 ramp down
	0x9d80, // 5 map next
	0xa002, // 6 loop to 2
	0x000a, // 7 - empty placeholder
	0x0005, // 8 - empty placeholder
	0x000a, // 9 - empty placeholder
	0x0005, // a - empty placeholder
	0x000a, // b - empty placeholder
	0x0005, // c - empty placeholder
	0x000a, // d - empty placeholder
	0x0005, // e - empty placeholder
	0x000a, // f - empty placeholder
	0x0001, // 10 map begin - start of 2nd page
	0x0002, // 11
	0x0004, // 12
	0x0008, // 13
	0x0010, // 14
	0x0020, // 15
	0x0040, // 16
	0x0080, // 17
	0x0040, // 18
	0x0020, // 19
	0x0010, // 1a
	0x0008, // 1b
	0x0004, // 1c
	0x0002, // 1d map end
};

static const uint16_t program_ramp_all[] =
{
	0x9c10, // 0 map start
	0x06ff, // 1 ramp up
	0x0200, // 2 wait
	0x07ff, // 3 ramp down
	0xa001, // 4 loop to 1
	0x000a, // 5 - empty placeholder
	0x0005, // 6 - empty placeholder
	0x000a, // 7 - empty placeholder
	0x0005, // 8 - empty placeholder
	0x000a, // 9 - empty placeholder
	0x0005, // a - empty placeholder
	0x000a, // b - empty placeholder
	0x0005, // c - empty placeholder
	0x000a, // d - empty placeholder
	0x0005, // e - empty placeholder
	0x000a, // f - empty placeholder
	0x00ff, // 10 map begin - start of 2nd page
};

static const uint16_t program_flash_sequentially[] =
{
	0x9c10, // 0 map start (base 0x9c00)
	0x9c97, // 1 map end (base 0x9c80)
	0x40ff, // 2 set pwm 255	
	0x7e00, // 3 wait
	0x4000, // 4 set pwm 0
	0x9d80, // 5 map next
	0xa002, // 6 loop to 2
	0x000a, // 7 - empty placeholder
	0x0005, // 8 - empty placeholder
	0x000a, // 9 - empty placeholder
	0x0005, // a - empty placeholder
	0x000a, // b - empty placeholder
	0x0005, // c - empty placeholder
	0x000a, // d - empty placeholder
	0x0005, // e - empty placeholder
	0x000a, // f - empty placeholder
	0x0001, // 10 map begin - start of 2nd page
	0x0002, // 11
	0x0004, // 12
	0x0008, // 13
	0x0010, // 14
	0x0020, // 15
	0x0040, // 16
	0x0080, // 17 map end
};

#endif // !LED_DRIVER_H_