#ifndef SYSTEM_SERVICE_H_
#define SYSTEM_SERVICE_H_

#include "bleSupport.h"

/*******************************************************************************************
 * @brief: Initializes the system service with all its characteristics
 *******************************************************************************************/
void ble_system_service_init(void);

/*******************************************************************************************
 * @brief: Function for handling ble events relevant to the system service
 * @param[in] p_ble_evt		Event received from the BLE stack
 *******************************************************************************************/
void ble_system_service_on_ble_evt(ble_evt_t* p_ble_evt);

/*******************************************************************************************
 * @brief: Notifies client of a system error (if client is subscribed to the error messages
 * characteristic).
 * @param[in]	err_code	Error code, describes in detail what the error is (see defines
							below)
 * @param[in]	str			Optional string which will be appended to the error code. If str 
 *							is NULL, no message will be appended. 
 *							SYSTEM_SERVICE_ERR_MSG_CHAR_MAX_LENGTH sets the maximum error 
 *							string length.
 *******************************************************************************************/
void ble_system_throw_error(uint16_t err_code, char* str);

#define SYSTEM_SERVICE_ERR_MSG_CHAR_MAX_LENGTH	32

// TODO: We need INFORMATION and DEBUG passing as well!

// System error code bases
#define IRIS_SUCCESS							0x0000
#define IRIS_ERROR_LP55231_BASE					0x0100
#define IRIS_ERROR_ADS1292_BASE					0x0200
#define IRIS_ERROR_BIOSENSE_BASE				0x0300

// LP55231 errors
#define IRIS_ERROR_LP55231_UNRESPONSIVE			(IRIS_ERROR_LP55231_BASE + 0x00)

// ADS1292 errors
#define IRIS_ERROR_ADS1292_UNRESPONSIVE			(IRIS_ERROR_ADS1292_BASE + 0x00)

// Biosense errors
#define IRIS_ERROR_BIOSENSE_TIMEOUT_OVERLAP		(IRIS_ERROR_BIOSENSE_BASE + 0x00)
#define IRIS_ERROR_BIOSENSE_CALIBRATE			(IRIS_ERROR_BIOSENSE_BASE + 0x01)

#define IRIS_ERROR_LED_UNKNOWN_PARAMETER		(IRIS_ERROR_BIOSENSE_BASE + 0x01)
#define IRIS_ERROR_LED_OUT_OF_BOUNDS			(IRIS_ERROR_BIOSENSE_BASE + 0x02)
#define IRIS_ERROR_LED_GROUP_UNKNOWN_PARAMETER	(IRIS_ERROR_BIOSENSE_BASE + 0x03)
#define IRIS_ERROR_LED_GROUP_OUT_OF_BOUNDS		(IRIS_ERROR_BIOSENSE_BASE + 0x04)

#endif // !SYSTEM_SERVICE_H_