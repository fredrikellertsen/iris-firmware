#include "biosenseHandler.h"

/* The biosense handler controls both the ADC and the LED driver after the
 * system is initialized. It controls sampling and LED updating. Therefore
 * all bluetooth events regarding the optical sensor and the LED driver
 * go through this module.
 */

// Struct for the current state of biosense
typedef struct {
	volatile bool		is_running;			//< true if biosense is currently running
	volatile bool		auto_increment;		//< if true, do auto-increment of leds. if false, do not control LEDs. see MTP
	volatile bool		waiting_for_sample;	//< if true, biosense is waiting for sample. Used to detect overlap
	volatile uint32_t	loop_period_ticks;	//< ticks between biosense loops
	volatile uint8_t	current_LED_group;	//< keeps track of current LED group
}biosense_state_t;

static uint8_t			biosense_data[BIOSESE_DATA_TARGET_SIZE];
static uint16_t			biosense_data_index = 0;
static biosense_state_t bs_state;
static led_group_t*		m_LED_groups;
static uint8_t			number_of_LED_groups;
static void(*biosense_txrdy_cb)(uint8_t*);		//< Callback for when a biosense data chunk is ready to be sent

/*******************************************************************************************
 * @brief: Change the number of LED groups in the system. Reallocates m_LED_groups if new size.
 * @param[in] n New number of LED groups
  *******************************************************************************************/
static void set_n_led_groups(uint8_t n) {
	
	NRF_LOG_INFO("set_n_led_groups to %d (currently %d)\r\n", n, number_of_LED_groups);
	
	if (n == number_of_LED_groups) {
		return;
	}
	
	// turn all LEDs off. New LED group might not contain LEDs which are currently on
	lp55231_set_led_pwm(0xFF, 0x00);
	
	free(m_LED_groups);
	number_of_LED_groups = n;
	m_LED_groups = malloc(sizeof(led_group_t) * n);
	memset(m_LED_groups, 0, sizeof(led_group_t) * n);
	
}

/*******************************************************************************************
 * @brief: Callback for when sample data is ready. Will read data from the ADC into buffer
 * and turn off LEDs to conserve power. If buffer is full, it will call biosense_txrdy_cb with 
 * the buffer as a parameter.
  *******************************************************************************************/
static void biosense_on_data_ready_cb() {
	
	if (!bs_state.is_running) {
		NRF_LOG_ERROR("biosense_on_data_ready called when biosense was not running!\r\n");
		return;
	}
	
	bs_state.waiting_for_sample = false;
	ads1292_read_sample(biosense_data + biosense_data_index, 1);
	biosense_data_index += 3;
	
	if (bs_state.auto_increment) {
		lp55231_set_led_pwm(m_LED_groups[bs_state.current_LED_group].LED_mask, 0x00);
		nrf_delay_us(m_LED_groups[bs_state.current_LED_group].release_us);
		bs_state.current_LED_group = (bs_state.current_LED_group + 1) % number_of_LED_groups;
	}
	
	if (biosense_data_index == BIOSESE_DATA_TARGET_SIZE) {
		biosense_data_index = 0;
		biosense_txrdy_cb(biosense_data);
	}
	
}

/*******************************************************************************************
 * @brief: Callback for biosense timer timeout. Will iterate the LED group and start a 
 * conversion. If bs_state.stop_after_loop is set, it will stop further biosense loops.
  *******************************************************************************************/
static void biosense_timer_timeout_handler(void * p_context) {	

	// error handling
	if (!bs_state.is_running) {
		NRF_LOG_ERROR("Biosense timeout called when biosense not running!\r\n");
		return;
	} else if (bs_state.waiting_for_sample) {
		biosense_stop();
		NRF_LOG_ERROR("Biosense timeout before data was ready!\r\n");
		ble_system_throw_error(IRIS_ERROR_BIOSENSE_TIMEOUT_OVERLAP, "TIMEOUT BEFORE DATA READY");
		return;
	} else if (!bs_state.auto_increment) {
		NRF_LOG_ERROR("biosense_timer_timeout_handler: timer is running when not in auto inc mode!\r\n");
		return;
	}

	// start sampling on current LED group
	lp55231_set_led_pwm(m_LED_groups[bs_state.current_LED_group].LED_mask, 0xFF);
	nrf_delay_us(m_LED_groups[bs_state.current_LED_group].attack_us);
	ads1292_start(&biosense_on_data_ready_cb, SINGLE_SHOT);
	bs_state.waiting_for_sample = true;
}

/*******************************************************************************************
 * @brief: Callback for LED group calibration finished
  *******************************************************************************************/
static void biosense_on_calibration_finished(bool did_succeed) {
	NRF_LOG_INFO("biosense_on_calibration_finished: did_succeed = %d\r\n", did_succeed);
	if (!did_succeed) {
		// TODO: notify software. 
	}
}

void biosense_init(void(*txrdy_cb)(uint8_t*)) {

	biosense_txrdy_cb			= txrdy_cb;
	memset(&bs_state, 0, sizeof(biosense_state_t));
	bs_state.auto_increment		= true;
	bs_state.loop_period_ticks	= APP_TIMER_TICKS(BIOSENSE_LOOP_PERIOD_DEFAULT, APP_TIMER_PRESCALER);
	
	// Initialize LED groups to default values
	number_of_LED_groups = HW_N_LEDS;
	m_LED_groups = malloc(sizeof(led_group_t) * number_of_LED_groups);
	memset(m_LED_groups, 0, sizeof(led_group_t) * number_of_LED_groups);
	for (uint8_t gr = 0; gr < number_of_LED_groups; gr++) {
		m_LED_groups[gr].LED_mask |= (1 << gr);
	}
	
	// Biosense timer has to be app_timer_create()'d in THIS file. Moving it elsewhere gives 
	// error code 0x0008 on calls to app_timer_start() or app_timer_stop(). No solution found
	APP_TIMER_INIT(APP_TIMER_PRESCALER, APP_TIMER_OP_QUEUE_SIZE, false);
	app_timer_create(&biosense_timer_id, APP_TIMER_MODE_REPEATED, biosense_timer_timeout_handler);
	
	NRF_LOG_INFO("Biosense initialized\r\n");
	
}

void biosense_start(void) {

	biosense_data_index			= 0;
	bs_state.is_running			= true;
	bs_state.waiting_for_sample = false;
	
	if (bs_state.auto_increment) {
		// if biosense is set to auto-increment LED groups, we need to start the timer which handles LED group iterations
		NRF_LOG_INFO("Starting biosense. auto_increment is true => starting timer\r\n");
		app_timer_start(biosense_timer_id, bs_state.loop_period_ticks, NULL);
	} else {
		// if not, simply start the ADC with callback to the data ready function.
		NRF_LOG_INFO("Starting biosense. auto_increment is false => starting first sample\r\n");
		ads1292_start(&biosense_on_data_ready_cb, CONTINUOUS);
	}
	
}

void biosense_stop(void) {
	if (bs_state.is_running) {
		NRF_LOG_INFO("Stopping biosense.\r\n");
		app_timer_stop(biosense_timer_id);
		ads1292_stop();
		if (bs_state.auto_increment) {
			lp55231_set_led_pwm(0xFF, 0);
		}
		bs_state.is_running = false;
		bs_state.current_LED_group = 0;
	}
}

void biosense_set_led_group_param(uint8_t* data, uint8_t length) {
	
	// Special test for setting number of LED groups to circumvent the LED range error handling below
	if (data[0] == 0x01) {
		set_n_led_groups(data[1]);
		return;
	}
	
	uint8_t LED_group = data[1];
	if (LED_group > number_of_LED_groups) {
		NRF_LOG_ERROR("biosense_set_led_group_param: cannot set parameters for LED group %d (out of bounds)\r\n", LED_group);
		ble_system_throw_error(IRIS_ERROR_LED_GROUP_OUT_OF_BOUNDS, NULL);
	}
	
	switch (data[0]) {
	case 0x00: // LED group on/off
		if (bs_state.is_running && bs_state.auto_increment) {
			NRF_LOG_ERROR("Cannot set LED group PWM while biosense is running in auto increment mode!\r\n");
		} else {
			lp55231_set_led_pwm(m_LED_groups[LED_group].LED_mask, data[2]*0xFF);
		}
		break;
	case 0x02: // set LED mask for LED group
		NRF_LOG_INFO("set LED group %d's LED mask to %d\r\n", LED_group, data[2]);
		m_LED_groups[LED_group].LED_mask = data[2];
		break;
	case 0x03: // set attack for LED group
		m_LED_groups[LED_group].attack_us = ((uint16_t)data[2] << 8) + data[3];
		NRF_LOG_INFO("set LED group %d's attack (in us) to %d\r\n", LED_group, m_LED_groups[LED_group].attack_us);
		break;
	case 0x04: // set release for LED group
		m_LED_groups[LED_group].release_us = ((uint16_t)data[2] << 8) + data[3];
		NRF_LOG_INFO("set LED group %d's release (in us) to %d\r\n", LED_group, m_LED_groups[LED_group].attack_us);
		break;
	default:
		NRF_LOG_ERROR("Unknown command in biosense_set_led_group_param: %#06x\r\n", data[0]);
		ble_system_throw_error(IRIS_ERROR_LED_GROUP_UNKNOWN_PARAMETER, NULL);
		break;
	}

}

void biosense_set_led_param(uint8_t* data, uint8_t length) {
	// todo add check of index
	
	switch (data[0]) {
	case 0x00: // single LED on/off
		if (bs_state.is_running && bs_state.auto_increment) {
			NRF_LOG_ERROR("Cannot set LED PWM while biosense is running in auto increment mode!\r\n");
		} else {
			lp55231_set_led_pwm(data[1], data[2]*0xFF);
		}
		break;
	case 0x01: // set single LED current
		lp55231_set_led_current(data[1], data[2]);
		break;
	case 0x05: // calibrate LEDs
		// no calibration when biosense is running.
		if (bs_state.is_running) {
			ble_system_throw_error(0x01, "Stop biosense first");
			return;
		}
		calibrate_LED_group_currents(m_LED_groups, number_of_LED_groups, &biosense_on_calibration_finished);
		break;
	default:
		NRF_LOG_ERROR("Unknown command in biosense_set_led_param: %#06x\r\n", data[0]);
		break;
	}
}

void biosense_set_os_param(uint8_t* data, uint8_t length) {

	switch (data[0]) {
	case 0x00: // set sample rate. TODO MAKE CHECK
		ads1292_set_sample_rate(data[1]);
		break;
	case 0x01: // set PGA gain
		ads1292_set_pga_gain(data[1]);
		break;
	case 0x02: // perform offset calibration
		ads1292_perform_offset_calibration();
		break;			
	case 0x03: { // automatic LED group incrementation?
			bool was_running = bs_state.is_running;
			if (was_running) {
				biosense_stop();
			}
			NRF_LOG_INFO("biosense_set_os_param: autoincrement = %d\r\n", data[1]);
			bs_state.auto_increment = data[1];
			if (was_running) {
				biosense_start();
			}
			break;
		}
	case 0x04: // set biosense loop interval
		NRF_LOG_INFO("Setting biosense loop interval: %d * 100 us\r\n", data[1]);
		bs_state.loop_period_ticks = APP_TIMER_TICKS((float)data[1]*0.1, APP_TIMER_PRESCALER);
		if (bs_state.is_running) {
			// restart the timer to apply the new timer interval
			app_timer_stop(biosense_timer_id);
			app_timer_start(biosense_timer_id, bs_state.loop_period_ticks, NULL);
		}
		break;
	default: // unknown parameter
		NRF_LOG_ERROR("Unknown parameter set in biosense_set_os_param: %#06x\r\n", data[0]);
	}

}