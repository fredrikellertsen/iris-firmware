// Project includes
#include "board_config.h"
#include "bleSupport.h"
#include "biosenseHandler.h"
#include "timing.h"
#include "ads1292.h"
#include "lp55231.h"
#include "systemService.h"
#include "deviceInfoService.h"
#include "battery.h"	
#include "icm20648.h"
#include "ble_dfu.h"
#include "ledService.h"
#include "osService.h"
#include "temperature.h"
#include "ble_dfu.h"

/* List of all services in the system. Battery service is local to
 * the battery module and not listed here due to the battery module's
 * shitty construction.
 */
ble_leds_service_t			m_leds_service;
ble_os_service_t			m_os_service;
ble_temperature_service_t	m_temp_service;
ble_device_info_service_t	m_device_info_service;
static ble_dfu_t			m_dfus;								/**< Structure used to identify the DFU service. */

// UUIDs of all services in the system, used for advertising
ble_uuid_t m_adv_uuids[] = { 
	{ BLE_UUID_SYSTEM_SRVC, BLE_UUID_TYPE_BLE},
	{ BLE_UUID_BATTERY_SRVC, BLE_UUID_TYPE_BLE},
	{ BLE_UUID_LED_SRVC, BLE_UUID_TYPE_BLE}, 
	{ BLE_UUID_OPTICAL_SENSOR_SRVC, BLE_UUID_TYPE_BLE},
	{ BLE_UUID_TEMPERATURE_SERVICE, BLE_UUID_TYPE_BLE}
};

static uint16_t m_conn_handle = BLE_CONN_HANDLE_INVALID;		/**< Handle of the current connection. */
static bool is_charging;										/**< True when charging. Checked at start of main */

#ifdef USES_DFU
#if (USES_DFU == true)
static void ble_dfu_evt_handler(ble_dfu_t * p_dfu, ble_dfu_evt_t * p_evt) {
	switch (p_evt->type) {
	case BLE_DFU_EVT_INDICATION_DISABLED:
		NRF_LOG_INFO("Indication for BLE_DFU is disabled\r\n");
		break;

	case BLE_DFU_EVT_INDICATION_ENABLED:
		NRF_LOG_INFO("Indication for BLE_DFU is enabled\r\n");
		break;

	case BLE_DFU_EVT_ENTERING_BOOTLOADER:
		NRF_LOG_INFO("Device is entering bootloader mode!\r\n");
		break;
	default:
		NRF_LOG_INFO("Unknown event from ble_dfu\r\n");
		break;
	}
}
#endif
#endif

/*******************************************************************************************
 * @brief: Function for starting advertising.
 *******************************************************************************************/
void advertising_start(void) {
	uint32_t err_code = ble_advertising_start(BLE_ADV_MODE_FAST);
	APP_ERROR_CHECK(err_code);
}

/*******************************************************************************************
 * @brief: Function for putting the chip into sleep mode. This function will not return.
 *******************************************************************************************/
void sleep_mode_enter(void) {
	uint32_t err_code;
	err_code = sd_power_system_off();
	APP_ERROR_CHECK(err_code);
}

/*******************************************************************************************
 * @brief: Callback function for asserts in the SoftDevice. This function will be called in 
 * case of an assert in the SoftDevice. This handler is an example only and does not fit a 
 * final product. You need to analyze how your product is supposed to react in case of Assert.
 * @param[in] line_num	Line number of the failing ASSERT call.
 * @param[in] file_name	File name of the failing ASSERT call.
 *******************************************************************************************/
void assert_nrf_callback(uint16_t line_num, const uint8_t * p_file_name) {
	NRF_LOG_ERROR("Assert fail\r\n");
	app_error_handler(0xDEADBEEF, line_num, p_file_name);
}

/*******************************************************************************************
 * @brief: Function for handling Peer Manager events.
 * @param[in] p_evt  Peer Manager event.
 *******************************************************************************************/
void pm_evt_handler(pm_evt_t const * p_evt) {
	ret_code_t err_code;

	switch (p_evt->evt_id) {
	case PM_EVT_BONDED_PEER_CONNECTED: {
			NRF_LOG_INFO("Connected to a previously bonded device.\r\n");
		} break;

	case PM_EVT_CONN_SEC_SUCCEEDED: {
			NRF_LOG_INFO("Connection secured. Role: %d. conn_handle: %d, Procedure: %d\r\n",
				ble_conn_state_role(p_evt->conn_handle),
				p_evt->conn_handle,
				p_evt->params.conn_sec_succeeded.procedure);
		} break;

	case PM_EVT_CONN_SEC_FAILED: {
		/* Often, when securing fails, it shouldn't be restarted, for security reasons.
		 * Other times, it can be restarted directly.
		 * Sometimes it can be restarted, but only after changing some Security Parameters.
		 * Sometimes, it cannot be restarted until the link is disconnected and reconnected.
		 * Sometimes it is impossible, to secure the link, or the peer device does not support it.
		 * How to handle this error is highly application dependent. */
		} break;

	case PM_EVT_CONN_SEC_CONFIG_REQ: {
		// Reject pairing request from an already bonded peer.
			pm_conn_sec_config_t conn_sec_config = { .allow_repairing = false };
			pm_conn_sec_config_reply(p_evt->conn_handle, &conn_sec_config);
		} break;

	case PM_EVT_STORAGE_FULL: {
		// Run garbage collection on the flash.
			err_code = fds_gc();
			if (err_code == FDS_ERR_BUSY || err_code == FDS_ERR_NO_SPACE_IN_QUEUES) {
				// Retry.
			} else {
				APP_ERROR_CHECK(err_code);
			}
		} break;

	case PM_EVT_PEERS_DELETE_SUCCEEDED: {
			advertising_start();
		} break;

	case PM_EVT_LOCAL_DB_CACHE_APPLY_FAILED: {
		// The local database has likely changed, send service changed indications.
			pm_local_database_has_changed();
		} break;

	case PM_EVT_PEER_DATA_UPDATE_FAILED: {
		// Assert.
			APP_ERROR_CHECK(p_evt->params.peer_data_update_failed.error);
		} break;

	case PM_EVT_PEER_DELETE_FAILED: {
		// Assert.
			APP_ERROR_CHECK(p_evt->params.peer_delete_failed.error);
		} break;

	case PM_EVT_PEERS_DELETE_FAILED: {
		// Assert.
			APP_ERROR_CHECK(p_evt->params.peers_delete_failed_evt.error);
		} break;

	case PM_EVT_ERROR_UNEXPECTED: {
		// Assert.
			APP_ERROR_CHECK(p_evt->params.error_unexpected.error);
		} break;

	case PM_EVT_CONN_SEC_START:
	case PM_EVT_PEER_DATA_UPDATE_SUCCEEDED:
	case PM_EVT_PEER_DELETE_SUCCEEDED:
	case PM_EVT_LOCAL_DB_CACHE_APPLIED:
	case PM_EVT_SERVICE_CHANGED_IND_SENT:
	case PM_EVT_SERVICE_CHANGED_IND_CONFIRMED:
	default:
		break;
	}
}

/*******************************************************************************************
 * @brief: Function for the GAP initialization. This function sets up all the necessary GAP 
 * (Generic Access Profile) parameters of the device including the device name, appearance, 
 * and the preferred connection parameters.
 *******************************************************************************************/
void gap_params_init(void) {
	uint32_t                err_code;
	ble_gap_conn_params_t   gap_conn_params;
	ble_gap_conn_sec_mode_t sec_mode;

	BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);

	err_code = sd_ble_gap_device_name_set(&sec_mode,
		(const uint8_t *)DEVICE_NAME,
		strlen(DEVICE_NAME));
	APP_ERROR_CHECK(err_code);

	// Set appearance
	err_code = sd_ble_gap_appearance_set(APPEARANCE);
	APP_ERROR_CHECK(err_code);
	
	memset(&gap_conn_params, 0, sizeof(gap_conn_params));

	gap_conn_params.min_conn_interval = MIN_CONN_INTERVAL;
	gap_conn_params.max_conn_interval = MAX_CONN_INTERVAL;
	gap_conn_params.slave_latency     = SLAVE_LATENCY;
	gap_conn_params.conn_sup_timeout  = CONN_SUP_TIMEOUT;

	err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
	APP_ERROR_CHECK(err_code);
}

/*******************************************************************************************
 * @brief: Function for handling the Connection Parameters Module. This function will be 
 * called for all events in the Connection Parameters Module which are passed to the application.
 * All this function does is to disconnect. This could have been done by simply setting the 
 * disconnect_on_fail config parameter, but instead we use the event handler mechanism to 
 * demonstrate its use.
 * @param[in] p_evt  Event received from the Connection Parameters Module.
 *******************************************************************************************/
void on_conn_params_evt(ble_conn_params_evt_t * p_evt) {
	uint32_t err_code;

	if (p_evt->evt_type == BLE_CONN_PARAMS_EVT_FAILED) {
		NRF_LOG_RAW_INFO("BLE_CONN_PARAMS_EVT_FAILED\r\n");
		err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_CONN_INTERVAL_UNACCEPTABLE);
		APP_ERROR_CHECK(err_code);
	}
}

/*******************************************************************************************
 * @brief: Function for handling a Connection Parameters error.
* @param[in] nrf_error  Error code containing information about what went wrong.
 *******************************************************************************************/
void conn_params_error_handler(uint32_t nrf_error) {
	APP_ERROR_HANDLER(nrf_error);
}

/*******************************************************************************************
 * @brief: Function for initializing the Connection Parameters module.
 * @param[in] ble_adv_evt  Advertising event.
 *******************************************************************************************/
void conn_params_init(void) {
	uint32_t               err_code;
	ble_conn_params_init_t cp_init;

	memset(&cp_init, 0, sizeof(cp_init));

	cp_init.p_conn_params                  = NULL;
	cp_init.first_conn_params_update_delay = FIRST_CONN_PARAMS_UPDATE_DELAY;
	cp_init.next_conn_params_update_delay  = NEXT_CONN_PARAMS_UPDATE_DELAY;
	cp_init.max_conn_params_update_count   = MAX_CONN_PARAMS_UPDATE_COUNT;
	cp_init.start_on_notify_cccd_handle    = BLE_GATT_HANDLE_INVALID;
	cp_init.disconnect_on_fail             = false;
	cp_init.evt_handler                    = on_conn_params_evt;
	cp_init.error_handler                  = conn_params_error_handler;

	err_code = ble_conn_params_init(&cp_init);
	APP_ERROR_CHECK(err_code);
}

/*******************************************************************************************
 * @brief: Function for handling advertising events.
 * @param[in] ble_adv_evt  Advertising event.
 *******************************************************************************************/
void on_adv_evt(ble_adv_evt_t ble_adv_evt) {

	switch (ble_adv_evt) {
	case BLE_ADV_EVT_FAST:
		NRF_LOG_INFO("Fast advertising\r\n");
		break;

	case BLE_ADV_EVT_IDLE:
		if (is_charging) {
			advertising_start(); // resume advertising if we are charging
		} else {
			sleep_mode_enter();
		}
		break;

	default:
		break;
	}
}

/*******************************************************************************************
 * @brief: Function for handling the Application's BLE Stack events.
 * @param[in] p_ble_evt  Bluetooth stack event.
 *******************************************************************************************/
void on_ble_evt(ble_evt_t * p_ble_evt) {
	uint32_t err_code = NRF_SUCCESS;

	switch (p_ble_evt->header.evt_id) {
	case BLE_GAP_EVT_DISCONNECTED:
		NRF_LOG_INFO("Disconnected.\r\n");
		break; // BLE_GAP_EVT_DISCONNECTED

	case BLE_GAP_EVT_CONNECTED:
		NRF_LOG_INFO("Connected. Requesting MTU size of %d bytes\r\n", 
			NRF_BLE_PREFERRED_MTU_SIZE);
		m_conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
	
		// Exchange preferred MTU size with client
		err_code = sd_ble_gattc_exchange_mtu_request(
			p_ble_evt->evt.gap_evt.conn_handle, 
			NRF_BLE_PREFERRED_MTU_SIZE);
		APP_ERROR_CHECK(err_code);
		break; // BLE_GAP_EVT_CONNECTED

	case BLE_GATTC_EVT_TIMEOUT:
		// Disconnect on GATT Client timeout event.
		NRF_LOG_DEBUG("GATT Client Timeout.\r\n");
		err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gattc_evt.conn_handle,
			BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
		APP_ERROR_CHECK(err_code);
		break; // BLE_GATTC_EVT_TIMEOUT

	case BLE_GATTS_EVT_TIMEOUT:
		// Disconnect on GATT Server timeout event.
		NRF_LOG_DEBUG("GATT Server Timeout.\r\n");
		err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gatts_evt.conn_handle,
			BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
		APP_ERROR_CHECK(err_code);
		break; // BLE_GATTS_EVT_TIMEOUT

	case BLE_EVT_USER_MEM_REQUEST:
		err_code = sd_ble_user_mem_reply(p_ble_evt->evt.gattc_evt.conn_handle, NULL);
		APP_ERROR_CHECK(err_code);
		break; // BLE_EVT_USER_MEM_REQUEST

	case BLE_GATTS_EVT_RW_AUTHORIZE_REQUEST: {
			ble_gatts_evt_rw_authorize_request_t  req;
			ble_gatts_rw_authorize_reply_params_t auth_reply;

			req = p_ble_evt->evt.gatts_evt.params.authorize_request;

			if (req.type != BLE_GATTS_AUTHORIZE_TYPE_INVALID) {
				if ((req.request.write.op == BLE_GATTS_OP_PREP_WRITE_REQ)     ||
					(req.request.write.op == BLE_GATTS_OP_EXEC_WRITE_REQ_NOW) ||
					(req.request.write.op == BLE_GATTS_OP_EXEC_WRITE_REQ_CANCEL)) {
					if (req.type == BLE_GATTS_AUTHORIZE_TYPE_WRITE) {
						auth_reply.type = BLE_GATTS_AUTHORIZE_TYPE_WRITE;
					} else {
						auth_reply.type = BLE_GATTS_AUTHORIZE_TYPE_READ;
					}
					auth_reply.params.write.gatt_status = APP_FEATURE_NOT_SUPPORTED;
					err_code = sd_ble_gatts_rw_authorize_reply(
						p_ble_evt->evt.gatts_evt.conn_handle,
						&auth_reply);
					APP_ERROR_CHECK(err_code);
				}
			}
		} break; // BLE_GATTS_EVT_RW_AUTHORIZE_REQUEST

#if (NRF_SD_BLE_API_VERSION == 3)
	case BLE_GATTS_EVT_EXCHANGE_MTU_REQUEST:
		NRF_LOG_INFO("Received BLE_GATTS_EVT_EXCHANGE_MTU_REQUEST \r\n");
		err_code = sd_ble_gatts_exchange_mtu_reply(p_ble_evt->evt.gatts_evt.conn_handle,
			NRF_BLE_PREFERRED_MTU_SIZE);
		APP_ERROR_CHECK(err_code);
		break; // BLE_GATTS_EVT_EXCHANGE_MTU_REQUEST
		
	case BLE_GATTC_EVT_EXCHANGE_MTU_RSP:
		NRF_LOG_INFO("Received BLE_CATTC_EVT_EXCHANGE_MTU_RSP, server_rx_MTU: %d \r\n", 
			p_ble_evt->evt.gattc_evt.params.exchange_mtu_rsp.server_rx_mtu);
		break;
		
	case BLE_EVT_DATA_LENGTH_CHANGED:
		// THIS IS DLE
		printf("Received BLE_EVT_DATA_LENGTH_CHANGED \r\n");
		printf("The maximum number of payload octets the LL expects to receive %d \r\n", 
			p_ble_evt->evt.common_evt.params.data_length_changed.max_rx_octets);
		printf("The maximum number of payload octets the LL will send %d \r\n", 
			p_ble_evt->evt.common_evt.params.data_length_changed.max_tx_octets);
		break;
#endif

	default:
		// No implementation needed.
		break;
	}
}

/*******************************************************************************************
 * @brief: Function for dispatching a BLE stack event to all modules with a BLE stack event 
 * handler. This function is called from the BLE Stack event interrupt handler after a BLE 
 * stack event has been received.
 * @param[in] p_ble_evt  Bluetooth stack event.
 *******************************************************************************************/
void ble_evt_dispatch(ble_evt_t* p_ble_evt) {
	ble_conn_state_on_ble_evt(p_ble_evt);
	pm_on_ble_evt(p_ble_evt);
	ble_conn_params_on_ble_evt(p_ble_evt);
	on_ble_evt(p_ble_evt);
	ble_advertising_on_ble_evt(p_ble_evt);
	ble_os_service_on_ble_evt(&m_os_service, p_ble_evt);
	ble_leds_service_on_ble_evt(&m_leds_service, p_ble_evt);
	ble_battery_measurement_on_ble_evt(p_ble_evt);
	ble_temperature_on_ble_evt(&m_temp_service, p_ble_evt);
	ble_dfu_on_ble_evt(&m_dfus, p_ble_evt);
	ble_system_service_on_ble_evt(p_ble_evt);
}

/*******************************************************************************************
 * @brief: Function for dispatching a system event to interested modules. This function is 
 * called from the System event interrupt handler after a system event has been received.
 * @param[in] sys_evt  System stack event.
 *******************************************************************************************/
void sys_evt_dispatch(uint32_t sys_evt) {
	// Dispatch the system event to the fstorage module, where it will be
	// dispatched to the Flash Data Storage (FDS) module.
	fs_sys_event_handler(sys_evt);

	// Dispatch to the Advertising module last, since it will check if there are any
	// pending flash operations in fstorage. Let fstorage process system events first,
	// so that it can report correctly to the Advertising module.
	ble_advertising_on_sys_evt(sys_evt);
}

/*******************************************************************************************
 * @brief: Function for initializing the BLE stack and BLE event interrupt.
 *******************************************************************************************/
void ble_stack_init(void) {
	uint32_t err_code;


	nrf_clock_lf_cfg_t clock_lf_cfg = NRF_CLOCK_LFCLKSRC;

	// Initialize the SoftDevice handler module.
	SOFTDEVICE_HANDLER_INIT(&clock_lf_cfg, NULL);

	ble_enable_params_t ble_enable_params;
	ble_enable_params.gatts_enable_params.service_changed = IS_SRVC_CHANGED_CHARACT_PRESENT;
	err_code = softdevice_enable_get_default_config(CENTRAL_LINK_COUNT,
		PERIPHERAL_LINK_COUNT,
		&ble_enable_params);
	APP_ERROR_CHECK(err_code);
	
	// Increase vs uuid cnt to accommodate the DFU service 
	ble_enable_params.common_enable_params.vs_uuid_count = 2; 

	// Increase the size of the GATT attr table size (trial and error)
	ble_enable_params.gatts_enable_params.attr_tab_size = 0x600; 
	
	// Check the ram settings against the used number of links
	CHECK_RAM_START_ADDR(CENTRAL_LINK_COUNT, PERIPHERAL_LINK_COUNT);

	// Enable BLE stack.
#if (NRF_SD_BLE_API_VERSION == 3)
	ble_enable_params.gatt_enable_params.att_mtu = NRF_BLE_PREFERRED_MTU_SIZE;
#endif
	err_code = softdevice_enable(&ble_enable_params);
	NRF_LOG_RAW_INFO("softdevice_enable: error code %d\n", err_code);
	
	// ugly way of processing all NRF_LOGs from softdevice_enable
	if (err_code != NRF_SUCCESS) {
		for (;;) {
			NRF_LOG_PROCESS();
		}
	}
	APP_ERROR_CHECK(err_code);

	// Register with the SoftDevice handler module for BLE events.
	err_code = softdevice_ble_evt_handler_set(ble_evt_dispatch);
	APP_ERROR_CHECK(err_code);

	// Register with the SoftDevice handler module for BLE events.
	err_code = softdevice_sys_evt_handler_set(sys_evt_dispatch);
	APP_ERROR_CHECK(err_code);
}

/*******************************************************************************************
 * @brief: Function for the Peer Manager initialization.
 * @param[in] erase_bonds	Indicates whether bonding information should be cleared from
 *							persistent storage during initialization of the Peer Manager.
 *******************************************************************************************/
void peer_manager_init(bool erase_bonds) {
	ble_gap_sec_params_t sec_param;
	ret_code_t           err_code;

	err_code = pm_init();
	APP_ERROR_CHECK(err_code);

	if (erase_bonds) {
		err_code = pm_peers_delete();
		APP_ERROR_CHECK(err_code);
	}

	memset(&sec_param, 0, sizeof(ble_gap_sec_params_t));

	// Security parameters to be used for all security procedures.
	sec_param.bond           = SEC_PARAM_BOND;
	sec_param.mitm           = SEC_PARAM_MITM;
	sec_param.lesc           = SEC_PARAM_LESC;
	sec_param.keypress       = SEC_PARAM_KEYPRESS;
	sec_param.io_caps        = SEC_PARAM_IO_CAPABILITIES;
	sec_param.oob            = SEC_PARAM_OOB;
	sec_param.min_key_size   = SEC_PARAM_MIN_KEY_SIZE;
	sec_param.max_key_size   = SEC_PARAM_MAX_KEY_SIZE;
	sec_param.kdist_own.enc  = 1;
	sec_param.kdist_own.id   = 1;
	sec_param.kdist_peer.enc = 1;
	sec_param.kdist_peer.id  = 1;

	err_code = pm_sec_params_set(&sec_param);
	APP_ERROR_CHECK(err_code);

	err_code = pm_register(pm_evt_handler);
	APP_ERROR_CHECK(err_code);
}

/*******************************************************************************************
 * @brief: Function for initializing the Advertising functionality.
 *******************************************************************************************/
void advertising_init(void) {
	uint32_t               err_code;
	ble_advdata_t          advdata;
	ble_adv_modes_config_t options;

	// Build advertising data struct to pass into @ref ble_advertising_init.
	memset(&advdata, 0, sizeof(advdata));

	advdata.name_type               = BLE_ADVDATA_FULL_NAME;
	advdata.include_appearance      = true;
	advdata.flags                   = BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE;
	advdata.uuids_complete.uuid_cnt = sizeof(m_adv_uuids) / sizeof(m_adv_uuids[0]);
	advdata.uuids_complete.p_uuids  = m_adv_uuids;
	

	memset(&options, 0, sizeof(options));
	options.ble_adv_fast_enabled  = true;
	options.ble_adv_fast_interval = APP_ADV_INTERVAL;
	options.ble_adv_fast_timeout  = APP_ADV_TIMEOUT_IN_SECONDS;

	err_code = ble_advertising_init(&advdata, NULL, &options, on_adv_evt, NULL);
	APP_ERROR_CHECK(err_code);
}

/*******************************************************************************************
 * @brief: Function for the Power manager.
 *******************************************************************************************/
void power_manage(void) {
	uint32_t err_code = sd_app_evt_wait();
	APP_ERROR_CHECK(err_code);
}

/*******************************************************************************************
 * @brief: Initializes timers for battery and temperature measurements.
 *******************************************************************************************/
void timers_init(void) {
	
	uint32_t err_code;
	APP_TIMER_INIT(APP_TIMER_PRESCALER, APP_TIMER_OP_QUEUE_SIZE, false);
	
	err_code = app_timer_create(
		&temperature_timer_id, 
		APP_TIMER_MODE_REPEATED, 
		temperature_timer_timeout_handler);
	
	err_code += app_timer_create(
		&battery_measurement_timer_id, 
		APP_TIMER_MODE_REPEATED, 
		battery_measurement_timer_timeout_handler);
	
	NRF_LOG_INFO("Timers initialized with error code 0x%#06\r\n", err_code);

}

/*******************************************************************************************
 * @brief: Function for initializing services
 *******************************************************************************************/
void services_init(void) {
	
	ble_system_service_init();
	ble_device_info_srv_init(&m_device_info_service);
	ble_temperature_service_init(&m_temp_service);
	ble_os_service_init(&m_os_service);
	ble_leds_service_init(&m_leds_service);
	
#ifdef USES_DFU
#if (USES_DFU == true)
	// Initialize the Device Firmware Update Service.
	uint32_t err_code;
	ble_dfu_init_t dfus_init;

	memset(&dfus_init, 0, sizeof(dfus_init));

	dfus_init.evt_handler                               = ble_dfu_evt_handler;
	dfus_init.ctrl_point_security_req_write_perm        = SEC_SIGNED;
	dfus_init.ctrl_point_security_req_cccd_write_perm   = SEC_SIGNED;

	err_code = ble_dfu_init(&m_dfus, &dfus_init);
	APP_ERROR_CHECK(err_code);
	NRF_LOG_INFO("DFU init successfull\r\n")
#endif
#endif
	 
}

/*******************************************************************************************
 * @brief: Callback to handle finished biosense loop data packets. Will call 
 * ble_os_service_notify with the data.
 *******************************************************************************************/
static void biosense_on_loop_finished_cb(uint8_t* data) {
	ble_os_service_notify(&m_os_service, data);
}

/*******************************************************************************************
 * @brief: Main
 *******************************************************************************************/
int main(void) {
	uint32_t err_code;
	bool     erase_bonds = false;

	// Initialize logging
	err_code = NRF_LOG_INIT(NULL);
	APP_ERROR_CHECK(err_code);

#ifdef BREADBOARD
	NRF_LOG_RAW_INFO("\r\n\r\n=~=~=~=Iris breadboard firmware started=~=~=~=\r\n");
#endif
#ifdef REV1
	NRF_LOG_RAW_INFO("\r\n\r\n=~=~=~=Iris REV 1 firmware started=~=~=~=\r\n");
#endif

	// Check if charging
	nrf_gpio_cfg_input(PIN_BAT_CHG, NRF_GPIO_PIN_PULLUP);
	if (!nrf_gpio_pin_read(PIN_BAT_CHG)) {
		is_charging = true;
		NRF_LOG_RAW_INFO("Charging\r\n");
	} else {
		NRF_LOG_RAW_INFO("Not charging\r\n");
		is_charging = false;
	}	

#ifdef REV1
	// Set STM HOLD high to enable power to rest of the system
	nrf_delay_ms(3000);
	nrf_gpio_cfg_output(PIN_STM_HOLD);
	nrf_gpio_pin_set(PIN_STM_HOLD);
#endif
	
	// Do startup flash
	lp55231_init();	
	for (uint8_t i = 0; i < 8; i++) {
		lp55231_set_led_pwm(1 << i, 255);
		nrf_delay_ms(50);
		lp55231_set_led_pwm(1 << i, 0);
	}

	// Initialize system and modules
	timers_init();
	ble_stack_init();
	peer_manager_init(erase_bonds);
	if (erase_bonds == true) {
		NRF_LOG_INFO("Bonds erased!\r\n");
	}
	gap_params_init();
	advertising_init();
	services_init();
	ads1292_init();
	biosense_init(biosense_on_loop_finished_cb);
	conn_params_init();
	battery_measurement_init();
	
	// start timers and advertising
	timer_start(temperature_timer_id, 10000, &m_temp_service);
	timer_start(battery_measurement_timer_id, 10000, NULL);
	err_code = ble_advertising_start(BLE_ADV_MODE_FAST);
	APP_ERROR_CHECK(err_code);
	
	// Main loop
	for (;;){
		// Process buffered log entries
		if (NRF_LOG_PROCESS() == false){
			power_manage();
		}
	}
	
}





/*	=== TODO ===
 *	- ASK JULIAN why does the charge sense thing not work
 *	- if sample rate changes, exchange new MTU
 *	- make sure no samples are lost during transfer or when buffer resets
 *
 *	- test led group integration time
 *	- test the accelerometer/gyro by switching the TWI interface.
*/