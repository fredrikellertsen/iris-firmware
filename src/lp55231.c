// SDK includes
#include "app_error.h"
#include "app_util_platform.h"
#include "nrf_drv_twi.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_gpio.h"
#include "nrf_delay.h"

// Project includes
#include "lp55231.h"

// keeps track of the current state of the LP55231 module
typedef struct {
	bool init_failed;
}lp55231_state_t;
static lp55231_state_t m_state;

static const nrf_drv_twi_t	m_twi_master	= NRF_DRV_TWI_INSTANCE(LP55231_TWI_INSTANCE);
static volatile bool		twi_xfer_done	= true;
static volatile bool		twi_xfer_failed = false;

/******************************************************************************************
 * @brief: Callback for twi event on LP55231_TWI_INSTANCE
 ******************************************************************************************/
static void twi_event_handler(nrf_drv_twi_evt_t const * p_event, void * p_context) {
	
	switch (p_event->type) {
	case NRF_DRV_TWI_EVT_DONE:
		twi_xfer_done = true;
		break;
	case NRF_DRV_TWI_EVT_ADDRESS_NACK:
	case NRF_DRV_TWI_EVT_DATA_NACK:
		NRF_LOG_ERROR("twi evt address/data nack\n");
		twi_xfer_failed = true;
		break;
	default:
		NRF_LOG_ERROR("unknown evt type: %d\n", p_event->type);
		break;
	}
}

 /******************************************************************************************
 * @brief:	Writes bytes to LP55231. First byte must be the address of the register to start 
 *			writing into. This function is blocking.
 * @param[in] data	Pointer to the data
 * @param[in] data	Length of the data to be written
 * @retval			True if write successful, false if not.
 ******************************************************************************************/
static void lp55231_write(uint8_t* data, uint8_t length) {
	
	uint32_t err_code;
	twi_xfer_done = false;
	err_code = nrf_drv_twi_tx(&m_twi_master, LP55231_ADDRESS, data, length, false);
	if (err_code != NRF_SUCCESS) {
		NRF_LOG_ERROR("twi tx returned err_code = %#06x\n", err_code);
	}
	while (!twi_xfer_done && !twi_xfer_failed);
	if (twi_xfer_failed) {
		ble_system_throw_error(IRIS_ERROR_LP55231_UNRESPONSIVE, "address/data nack");
	}
	
}

 /******************************************************************************************
 * @brief: Reads bytes from LP55231
 * @param[in] reg The register to start reading from
 * @param[in] length The number of bytes to read
 * @param[in] res Pointer to array in which to put the bytes
 ******************************************************************************************/
static void lp55231_read(uint8_t reg, uint8_t length, uint8_t* res) {
	
	uint32_t err_code;
	
	lp55231_write(&reg, 1);
	
	twi_xfer_done = false;
	err_code = nrf_drv_twi_rx(&m_twi_master, LP55231_ADDRESS, res, length);
	if (err_code != NRF_SUCCESS) {
		NRF_LOG_ERROR("twi rx returned err_code = %#06x\n", err_code);
	}
	while (!twi_xfer_done && !twi_xfer_failed);
	if (twi_xfer_failed) {
		ble_system_throw_error(IRIS_ERROR_LP55231_UNRESPONSIVE, "address/data nack");
	}
	
}

void lp55231_init(void) {
	
	uint32_t err_code;
	memset(&m_state, 0, sizeof(lp55231_state_t));
	m_state.init_failed = false;
	
	const nrf_drv_twi_config_t twi_lp55231 = { 
		.scl = PIN_LP55231_SCL,
		.sda = PIN_LP55231_SDA,
		.frequency = NRF_TWI_FREQ_400K,
		.interrupt_priority = APP_IRQ_PRIORITY_HIGH,
		.clear_bus_init = false,
	};
	
	err_code = nrf_drv_twi_init(&m_twi_master, &twi_lp55231, twi_event_handler, NULL);
	if (err_code != NRF_SUCCESS) {
		NRF_LOG_ERROR("lp55231_init: twi_init returned err_code = %#06x\n", err_code);
		ble_system_throw_error(IRIS_ERROR_LP55231_UNRESPONSIVE, "TWI INIT FAILED");
		return;
	}
	nrf_drv_twi_enable(&m_twi_master);

	nrf_gpio_cfg_output(PIN_LP55231_EN);
	nrf_gpio_cfg_output(PIN_LP55231_CLK);
	nrf_gpio_pin_set(PIN_LP55231_EN);

	// Reset LP55231. No ACK will follow.
	uint8_t data[2];
	data[0] = LP55231_REG_RESET;
	data[1] = 0xFF;
	lp55231_write(data, sizeof(data) / sizeof(uint8_t));
	if (twi_xfer_failed) {
		ble_system_throw_error(IRIS_ERROR_LP55231_UNRESPONSIVE, "TWI TX FAILED");
		m_state.init_failed = true;
		return;
	}
	
	// Reinitialize the TWI driver as NACK breaks the nrf twi driver...
	nrf_drv_twi_uninit(&m_twi_master);
	err_code = nrf_drv_twi_init(&m_twi_master, &twi_lp55231, twi_event_handler, NULL);
	if (err_code != NRF_SUCCESS) {
		NRF_LOG_ERROR("lp55231_init: twi re-init with with err_code = %#06x\n", err_code);
		ble_system_throw_error(IRIS_ERROR_LP55231_UNRESPONSIVE, "TWI REINIT FAILED");	
		return;
	}
	nrf_drv_twi_enable(&m_twi_master);
	
	// Set CHIP_EN bit in ENABLE register	
	data[0] = LP55231_REG_CNTRL1;
	data[1] = LP55231_ENABLE;
	lp55231_write(data, sizeof(data) / sizeof(uint8_t));
	
	// The datasheet (p. 17) calls for 500 us startup delay after CHIP_EN. 1000 is safe
	nrf_delay_us(1000);

	// Set misc: Charge pump, auto increment and clock source selection
	data[0] = LP55231_REG_MISC;
	data[1] = (LP55231_CP_1X5 | LP55231_INT_CLK | LP55231_AUTO_INC);
	lp55231_write(data, sizeof(data) / sizeof(uint8_t));

	// Set LED currents to default values
	lp55231_set_led_current(0xFF, HW_LED_CURRENT_DEFAULT);

	NRF_LOG_INFO("lp55231 init complete\r\n")
	
}

void lp55231_set_led_current(uint8_t LED_mask, uint8_t current) {
	
	if (m_state.init_failed) {
		return;
	}
	
	if (current > HW_LED_CURRENT_MAX) {
		NRF_LOG_ERROR("lp55231_set_led_current: LED current is too high (%d * 100 uA)\n", current);
		current = HW_LED_CURRENT_MAX;
	}
	
	// Loop through LED mask and apply current changes
	uint8_t data[2];
	for (uint8_t i = 0; i < 8; i++) {
		if ((1 << i) & LED_mask) {
			data[0] = LP55231_REG_LED_CURRENT_BASE + i;
			data[1] = current;
			lp55231_write(data, sizeof(data) / sizeof(uint8_t));
		}
	}
		
}

void lp55231_set_led_pwm(uint8_t LED_mask, uint8_t pwm_value) {
	
	if (m_state.init_failed) {
		return;
	}
	
	uint8_t data[2];
	for (uint8_t i = 0; i < 8; i++) {
		if ((1 << i) & LED_mask) {
			data[0] = LP55231_REG_LED_PWM_BASE + i;
			data[1] = pwm_value;
			lp55231_write(data, sizeof(data) / sizeof(uint8_t));
		}
	}

}

void lp55231_load_program(const uint16_t* program_16b, uint16_t length) {
	
	if (m_state.init_failed) {
		return;
	}
	
	uint8_t cmd_loadProg[]		= { LP55231_REG_CNTRL2, 0x15 };
	uint8_t cmd_pageSel[]		= { LP55231_REG_PROGMEM_PAGESEL, 0x00 };
	uint8_t cmd_disableEng[]	= { LP55231_REG_CNTRL2, 0x00 };
	
	// Enter program load mode (engine 1)
	lp55231_write(cmd_disableEng, 2);
	lp55231_write(cmd_loadProg, 2);
	
	// wait for the engine bus bit to clear, indicating that write is possible
	uint8_t status;
	do {
		nrf_delay_ms(1);
		lp55231_read(LP55231_REG_ENG_STATUS, 1, &status);
	} while (status & 0x10);

	// write program into SRAM, page by page
	uint8_t cmd[3];
	uint8_t pageAddr = 0;
	for (uint32_t instr = 0; instr < length; instr++) {
		
		if (( instr % (LP55231_PAGE_SIZE) ) == 0) {
			cmd_pageSel[1] = instr / LP55231_PAGE_SIZE;
			lp55231_write(cmd_pageSel, 2);
			pageAddr = 0;
		}
		
		cmd[0] = LP55231_REG_PROGMEM_START + pageAddr;
		cmd[1] = (program_16b[instr] >> 8) & 0xFF;
		cmd[2] = program_16b[instr] & 0xFF;
		lp55231_write(cmd, 3);
		
		pageAddr += 2;
	}

	lp55231_write(cmd_disableEng, 2);
	
}

void lp55231_run_program(void) {
	
	if (m_state.init_failed) {
		return;
	}

	uint8_t cmd_entryPtzero[]	= { LP55231_REG_ENG_ENTRY_POINT, 0x00 };
	uint8_t cmd_setPCzero[]		= { LP55231_REG_ENG_PC_BASE, 0x00 };
	uint8_t cmd_runEng1[]		= { LP55231_REG_CNTRL2, 0x20 };
	uint8_t cmd_freeRunEng1[]	= { LP55231_REG_CNTRL1, 0x60 };

	lp55231_write(cmd_entryPtzero, 2);	// set engine program start addr to 0 for all engines
	lp55231_write(cmd_setPCzero, 2);	// set engine program counter
	lp55231_write(cmd_freeRunEng1, 2);	// set engine mode free
	lp55231_write(cmd_runEng1, 2);		// start engine
	
}
