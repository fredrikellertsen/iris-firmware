#include "systemService.h"
#include "nrf_error.h"

static ble_system_service_t* m_system_service;
static bool is_initialized = false;

void ble_system_service_init(void) {
	uint32_t err_code;
	
	m_system_service = malloc(sizeof(ble_system_service_t));
	memset(m_system_service, 0, sizeof(ble_system_service_t));
	m_system_service->conn_handle	= BLE_CONN_HANDLE_INVALID;
	
	// Declare 16-bit service and 128-bit base UUIDs and add them to the BLE stack
	ble_uuid_t				system_srvc_uuid;
	system_srvc_uuid.uuid	= BLE_UUID_SYSTEM_SRVC;
	ble_uuid128_t base_uuid = BLE_UUID_MTP_BASE;
	
	// Add the system service UUID to the SD
	err_code = sd_ble_uuid_vs_add(&base_uuid, &system_srvc_uuid.type);
	APP_ERROR_CHECK(err_code);
	
	err_code = sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY,
		&system_srvc_uuid,
		&m_system_service->service_handle);	
	APP_ERROR_CHECK(err_code);
	
	// Add the system control point characteristic
	m_system_service->ctrl_pt_char.char_uuid.uuid			= BLE_UUID_SYSTEM_CTRL_PT;
	m_system_service->ctrl_pt_char.char_props.write			= true;
	m_system_service->ctrl_pt_char.char_props.length		= 10;
	ble_add_characteristic(m_system_service->service_handle, &m_system_service->ctrl_pt_char);
	
	// Add the system control point response characteristic
	m_system_service->ctrl_pt_resp_char.char_uuid.uuid		= BLE_UUID_SYSTEM_CTRL_PT_RESP;
	m_system_service->ctrl_pt_resp_char.char_props.notify	= true;
	m_system_service->ctrl_pt_resp_char.char_props.length	= 10;
	ble_add_characteristic(m_system_service->service_handle, &m_system_service->ctrl_pt_resp_char);
	
	// Add the system error message characteristic
	m_system_service->err_msg_char.char_uuid.uuid			= BLE_UUID_SYSTEM_ERR;
	m_system_service->err_msg_char.char_props.notify		= true;
	m_system_service->err_msg_char.char_props.length		= SYSTEM_SERVICE_ERR_MSG_CHAR_MAX_LENGTH;
	ble_add_characteristic(m_system_service->service_handle, &m_system_service->err_msg_char);
	
	is_initialized = true;
	
	NRF_LOG_INFO("System service initialized\r\n");

}

/*******************************************************************************************
 * @brief: Handles writes to system service
  *******************************************************************************************/
static void ble_system_service_on_write(ble_gatts_evt_write_t* w_evt) {
	
	// check if write was to the os service control point
	if (w_evt->uuid.uuid == m_system_service->ctrl_pt_char.char_uuid.uuid) {
		// not implemented yet
	} 
	
	// check if write was to the system service error message char. cccd (=> subscription update)
	if (w_evt->handle == m_system_service->err_msg_char.char_handle.cccd_handle) {
		m_system_service->is_client_subscribed_to_errors = w_evt->data[0];
		NRF_LOG_INFO("ble_system_service_on_write: err msg client subscription update: %d\r\n", w_evt->data[0]);
	} else if(w_evt->handle == m_system_service->ctrl_pt_resp_char.char_handle.cccd_handle) {
		m_system_service->is_client_subscribed_to_ctrl_pt = w_evt->data[0];
		NRF_LOG_INFO("ble_system_service_on_write: ctrl pt subscription update: %d\r\n", w_evt->data[0]);
		NRF_LOG_INFO("\t\tNot implemented\r\n");
	}
	
}

void ble_system_service_on_ble_evt(ble_evt_t* p_ble_evt) {
	
	if (!is_initialized) {
		ble_system_service_init();
	}

	switch (p_ble_evt->header.evt_id) {
	case BLE_GAP_EVT_CONNECTED:
		m_system_service->conn_handle	= p_ble_evt->evt.gap_evt.conn_handle;
		break;
	case BLE_GAP_EVT_DISCONNECTED:
		m_system_service->conn_handle	= BLE_CONN_HANDLE_INVALID;
		break;
	case BLE_GATTS_EVT_WRITE:
		ble_system_service_on_write(&p_ble_evt->evt.gatts_evt.params.write);
		break;
	default:
		// no implementation needed
		break;
	}
}

void ble_system_throw_error(uint16_t err_code, char* str) {
	
	if (!is_initialized) {
		ble_system_service_init();
	}
	
	uint8_t* data;
	uint8_t str_len = 0;
	
	if (str != NULL) {
		do {
			str_len++;
		} while (str[str_len] != 0 && str_len <= SYSTEM_SERVICE_ERR_MSG_CHAR_MAX_LENGTH);
		NRF_LOG_INFO("Length of string was %d\r\n", str_len);
	}
	
	uint16_t data_len = 2 + str_len;
	data = malloc(data_len * sizeof(uint8_t));
	data[0] = (err_code >> 8) & 0xFF;
	data[1] = err_code & 0xFF;
	if (str_len > 0) {
		memcpy(data + 2, str, str_len);
	}
	
	if (m_system_service->conn_handle != BLE_CONN_HANDLE_INVALID && m_system_service->is_client_subscribed_to_errors) {

		ble_gatts_hvx_params_t	hvx_params;
		memset(&hvx_params, 0, sizeof(hvx_params));

		hvx_params.handle = m_system_service->err_msg_char.char_handle.value_handle;
		hvx_params.type   = BLE_GATT_HVX_NOTIFICATION;
		hvx_params.p_len  = &data_len;
		hvx_params.p_data = data;
		
		err_code = sd_ble_gatts_hvx(m_system_service->conn_handle, &hvx_params);	
		if (err_code != NRF_SUCCESS) {
			ble_log_hvx_err_code(err_code);
		}
		NRF_LOG_RAW_INFO("Notified err msg with err_code %#06x\r\n", err_code);		
	} else {	
		NRF_LOG_RAW_INFO("Did not notify err msg char, client unsubscribed or not connected\r\n ");		
	}
	
}
