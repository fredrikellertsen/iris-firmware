#include "battery.h"
#include "board_config.h"
#include "nrf_drv_saadc.h"

#define BAT_FULL_VOLTAGE				3.7
#define BAT_EMPTY_VOLTAGE				3.0
#define SAMPLE_TO_VOLT_MULTIPLIER		0.00839776 // 1/(2^resolution - 1)*(1/gain)*ref*1/(volatge divider ratio [4k4 and 6k1])
#define SAMPLE_TO_PERCENTAGE_MULTIPLIER	SAMPLE_TO_VOLT_MULTIPLIER * BAT_FULL_VOLTAGE

#define SAMPLES_IN_BUFFER				1
#define N_AVGS							10 // Number of samples to average when calculating bat voltage
static nrf_saadc_value_t				m_batsense_sample_buffer_pool[1][SAMPLES_IN_BUFFER];

static int16_t sampleBuf[N_AVGS];

static ble_bat_service_t m_bat_service;
static uint8_t current_battery_percentage;

/*******************************************************************************************
 * @brief: Converts an saadc sample value to a battery percentage (0-100)
 * @param[in]	sample	The saadc sample to use in the calculation
 * @param[out]	uint8_t	Percentage result
 *******************************************************************************************/
static uint8_t get_battery_level_percentage(int16_t sample) {
	float voltage = (float)sample * SAMPLE_TO_VOLT_MULTIPLIER;
	
	if (voltage >= BAT_FULL_VOLTAGE) {
		return 100;
	} else if (voltage <= BAT_EMPTY_VOLTAGE) {
		return 0;
	} else {
		return 100*(voltage - BAT_EMPTY_VOLTAGE) / (BAT_FULL_VOLTAGE - BAT_EMPTY_VOLTAGE);
	}

}

/*******************************************************************************************
 * @brief: Notifies the battery voltage to the client if he is connected and subscribed
 * @param[in]	battery level	Pointer to the battery level
 *******************************************************************************************/
static void battery_measurement_notify(uint8_t* battery_level) {
	
	if (m_bat_service.is_client_subscribed == false || m_bat_service.conn_handle == BLE_CONN_HANDLE_INVALID) {
		// client is unsubscribed or unconnected, return
		//NRF_LOG_RAW_INFO("battery_measurement_notify: client is unsubscribed\r\n");
		return;
	}

	ble_gatts_hvx_params_t	hvx_params;
	memset(&hvx_params, 0, sizeof(hvx_params));

	hvx_params.handle = m_bat_service.battery_level_char.char_handle.value_handle;
	hvx_params.type   = BLE_GATT_HVX_NOTIFICATION;
	hvx_params.p_len  = &m_bat_service.battery_level_char.char_props.length;
	hvx_params.p_data = battery_level;
		
	uint32_t err_code;
	err_code = sd_ble_gatts_hvx(m_bat_service.conn_handle, &hvx_params);	
	if (err_code != NRF_SUCCESS) {
		ble_log_hvx_err_code(err_code);
	}

}

/*******************************************************************************************
 * @brief: Callback function for saadc events. NRF_DRV_SAADC_EVT_DONE triggers new samples
 * until 10 samples have been taken, after which the battery level is estimated and 
 * notified to client.
  *******************************************************************************************/
static void saadc_callback(nrf_drv_saadc_evt_t const * p_event) {
	uint32_t err_code;
	static uint16_t cbCnt = 0;

	if (p_event->type == NRF_DRV_SAADC_EVT_DONE) {
		err_code = nrf_drv_saadc_buffer_convert(p_event->data.done.p_buffer, SAMPLES_IN_BUFFER);
		APP_ERROR_CHECK(err_code);
		
		sampleBuf[cbCnt] = p_event->data.done.p_buffer[0];

		cbCnt++;
		if (cbCnt < 10) {
			err_code = nrf_drv_saadc_sample();
		} else {
			cbCnt = 0;
			
			int32_t avgSample = 0;
			for (uint8_t i = 0; i < N_AVGS; i++) {
				avgSample += sampleBuf[i];
			}
			avgSample /= N_AVGS;
			current_battery_percentage = get_battery_level_percentage(avgSample);
			battery_measurement_notify(&current_battery_percentage);
		}
	}
}	

/*******************************************************************************************
 * @brief: Initializes the SAADC module 
  *******************************************************************************************/
static void saadc_init(void) {
	uint32_t err_code;
	nrf_drv_saadc_config_t saadc_config = NRF_DRV_SAADC_DEFAULT_CONFIG;
	nrf_saadc_channel_config_t channel_config = NRF_DRV_SAADC_DEFAULT_CHANNEL_CONFIG_SE(PIN_BATSENSE);
	
	// Initialize SAADC
	err_code = nrf_drv_saadc_init(&saadc_config, saadc_callback);
	APP_ERROR_CHECK(err_code);

	// Initialize SAADC channel
	err_code = nrf_drv_saadc_channel_init(0, &channel_config);
	APP_ERROR_CHECK(err_code);

	// Initialize ADC buffers
	err_code = nrf_drv_saadc_buffer_convert(m_batsense_sample_buffer_pool[0], SAMPLES_IN_BUFFER);
	APP_ERROR_CHECK(err_code);


}

static void ble_battery_service_on_write(ble_evt_t* p_ble_evt) {
	ble_gatts_evt_write_t w_evt	= p_ble_evt->evt.gatts_evt.params.write;

		// check if write was to the battery level cccd handle (=> subscription update)
	if (w_evt.handle == m_bat_service.battery_level_char.char_handle.cccd_handle) {
		if (w_evt.data[0] == 1) {
			NRF_LOG_INFO("Battery service: client subscribed\r\n");
			m_bat_service.is_client_subscribed = true;
		} else {
			NRF_LOG_INFO("Battery service: client unsubscribed\r\n");
			m_bat_service.is_client_subscribed = false;
		}
	}
}

void battery_measurement_init(void) {
	uint32_t   err_code;
	
	saadc_init();

	memset(&m_bat_service, 0, sizeof(ble_bat_service_t));
	m_bat_service.conn_handle	= BLE_CONN_HANDLE_INVALID;

	// Declare 16-bit service and 128-bit base UUIDs and add them to the BLE stack
	ble_uuid_t				bat_srvc_uuid;
	bat_srvc_uuid.uuid		= BLE_UUID_BATTERY_SRVC;
	ble_uuid128_t base_uuid = BLE_UUID_MTP_BASE;

	err_code = sd_ble_uuid_vs_add(&base_uuid, &bat_srvc_uuid.type);
	APP_ERROR_CHECK(err_code);    

	err_code = sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY,
		&bat_srvc_uuid,
		&m_bat_service.service_handle);
	APP_ERROR_CHECK(err_code);

	// Add leds control point characteristic	
	m_bat_service.battery_level_char.char_uuid.uuid		= BLE_UUID_BATTERY_LEVEL;
	m_bat_service.battery_level_char.char_props.read	= true;
	m_bat_service.battery_level_char.char_props.notify	= true;
	m_bat_service.battery_level_char.char_props.length	= 1;
	ble_add_characteristic(m_bat_service.service_handle, &m_bat_service.battery_level_char);

	NRF_LOG_INFO("Battery service service initialized\r\n");
}

void battery_measurement_timer_timeout_handler(void * p_context) {
	static uint8_t sampleCnt = 0;
	
	// Do offset calibration every 10 times
	if (sampleCnt > 10) {
		sampleCnt = 0;
		nrf_drv_saadc_calibrate_offset();
	}
	
	nrf_drv_saadc_sample();
	sampleCnt++;
}

void ble_battery_measurement_on_ble_evt(ble_evt_t* p_ble_evt) {
	
	switch (p_ble_evt->header.evt_id) {
	case BLE_GAP_EVT_CONNECTED:
		m_bat_service.conn_handle	= p_ble_evt->evt.gap_evt.conn_handle;
		break;
	case BLE_GAP_EVT_DISCONNECTED:
		m_bat_service.conn_handle	= BLE_CONN_HANDLE_INVALID;
		break;
	case BLE_GATTS_EVT_WRITE:
		ble_battery_service_on_write(p_ble_evt);
		break;
	}

}
