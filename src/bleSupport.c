#include "bleSupport.h"

void ble_add_characteristic(uint16_t service_handle, ble_char_t* ch) {
	
	// Add characteristic UUID to softdevice
	uint32_t		err_code;
	ble_uuid_t		char_uuid;
	ble_uuid128_t	base_uuid = BLE_UUID_MTP_BASE;
	char_uuid.uuid	= ch->char_uuid.uuid;
	err_code = sd_ble_uuid_vs_add(&base_uuid, &char_uuid.type);
	APP_ERROR_CHECK(err_code);
	
	// Add r/w/notify properties to the characteristic
	ble_gatts_char_md_t char_md;
	memset(&char_md, 0, sizeof(char_md));
	char_md.char_props.read		= ch->char_props.read;
	char_md.char_props.write	= ch->char_props.write;
	char_md.char_props.notify   = ch->char_props.notify;

	// Configure Client Characteristic Configuration Descriptor metadata and add to char_md structure
	ble_gatts_attr_md_t cccd_md;
	memset(&cccd_md, 0, sizeof(cccd_md));
	BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.read_perm);
	BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.write_perm);
	cccd_md.vloc                = BLE_GATTS_VLOC_STACK;    
	char_md.p_cccd_md           = &cccd_md;
	
	// Configure the attribute metadata
	ble_gatts_attr_md_t attr_md;
	memset(&attr_md, 0, sizeof(attr_md));  
	attr_md.vloc = BLE_GATTS_VLOC_STACK;
	
	// Set read/write security levels to the characteristic
	BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
	BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);
	
	// Configure the characteristic value attribute and assign initial value if available
	ble_gatts_attr_t    attr_char_value;
	memset(&attr_char_value, 0, sizeof(attr_char_value));
	attr_char_value.p_uuid		= &char_uuid;
	attr_char_value.p_attr_md	= &attr_md;
	
	// Set characteristic length in number of bytes
	attr_char_value.max_len		= ch->char_props.length;
	attr_char_value.init_len	= ch->char_props.length;
	
	// Add initial value of characteristic (if such is provided)
	if (ch->char_props.init_value != NULL) {
		attr_char_value.p_value	= ch->char_props.init_value;
	} else {
		memset(attr_char_value.p_value, 0, ch->char_props.length);
	}
	
	// Add the characteristic to the service (tell the SD about the new characteristic)
	err_code = sd_ble_gatts_characteristic_add(service_handle,
		&char_md,
		&attr_char_value,
		&ch->char_handle);
	if (err_code) {
		NRF_LOG_RAW_INFO("sd_ble_gatts_characteristic_add with err_code %d\n", err_code);
	}
	
}

void ble_log_hvx_err_code(uint32_t err_code) {
	NRF_LOG_ERROR("ble_check_hvx_err_code: ")
	switch (err_code) {
	case BLE_ERROR_NO_TX_PACKETS:
		// TX buffer is full
		NRF_LOG_ERROR("BLE_ERROR_NO_TX_PACKETS\r\n")
		break;
	case BLE_ERROR_GATTS_SYS_ATTR_MISSING:
		// bonding data is missing. This error goes away once central subscribes
		NRF_LOG_ERROR("BLE_ERROR_GATTS_SYS_ATTR_MISSING\r\n")
		break;
	case BLE_ERROR_GATTS_INVALID_ATTR_TYPE:
		// this is bad, attribute was improperly initialized
		NRF_LOG_ERROR("BLE_ERROR_GATTS_INVALID_ATTR_TYPE\r\n")
		break;
	case NRF_ERROR_INVALID_STATE:
		NRF_LOG_ERROR("NRF_ERROR_INVALID_STATE\r\n")
		break;
	default:
		NRF_LOG_ERROR("unknown error %#06\r\n", err_code)
		break;	
	}
}