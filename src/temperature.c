#include "temperature.h"
#include "nrf_soc.h"
#include "nrf_log.h"

/*******************************************************************************************
 * @brief: Notifies the client (if connected and subscribed) of the temperature. 
 * @param[in] temperature Array of bytes structured as described in get_temperature_c
 *******************************************************************************************/
static void ble_die_temperature_notify(ble_temperature_service_t* m_temp_service, uint8_t* temperature) {
	
	if (m_temp_service->is_client_subscribed_to_die_temp == false || m_temp_service->conn_handle == BLE_CONN_HANDLE_INVALID) {
		// client is unsubscribed or unconnected, return
		//NRF_LOG_RAW_INFO("ble_die_temperature_notify: client is unsubscribed or not connected\r\n");		
		return;
	}

	ble_gatts_hvx_params_t	hvx_params;
	memset(&hvx_params, 0, sizeof(hvx_params));

	hvx_params.handle = m_temp_service->die_temperature_char.char_handle.value_handle;
	hvx_params.type   = BLE_GATT_HVX_NOTIFICATION;
	hvx_params.p_len  = &m_temp_service->die_temperature_char.char_props.length;
	hvx_params.p_data = temperature;
		
	uint32_t err_code;
	err_code = sd_ble_gatts_hvx(m_temp_service->conn_handle, &hvx_params);	
	if (err_code != NRF_SUCCESS) {
		ble_log_hvx_err_code(err_code);
	}
}

/*******************************************************************************************
 * @brief: Handles writes to the die temperature characteristic
  *******************************************************************************************/
static void ble_die_temperature_on_write(ble_temperature_service_t* m_temp_service, ble_evt_t* p_ble_evt) {
	
	ble_gatts_evt_write_t w_evt	= p_ble_evt->evt.gatts_evt.params.write;
	
	// check if write was to the leds service control point
	if (w_evt.uuid.uuid == m_temp_service->temperature_ctrl_point.char_uuid.uuid) {
		NRF_LOG_INFO("ble_die_temperature_on_write: ctrl pt write: ");
		NRF_LOG_RAW_HEXDUMP_INFO(w_evt.data, w_evt.len);

		// TODO: HANDLE INTERVAL UPDATE HERE

	}	
	
	// check if write was to the die temperature characteristic cccd handle (=> subscription update)
	if (w_evt.handle == m_temp_service->die_temperature_char.char_handle.cccd_handle) {
		if (w_evt.data[0] == 1) {
			NRF_LOG_INFO("Die temperature: client subscribed\r\n");
			m_temp_service->is_client_subscribed_to_die_temp = true;
		} else {
			NRF_LOG_INFO("Die temperature: client unsubscribed \r\n");
			m_temp_service->is_client_subscribed_to_die_temp = false;
		}
	}
	
}

void ble_temperature_service_init(ble_temperature_service_t* m_temp_service) {
	uint32_t   err_code;
	
	memset(m_temp_service, 0, sizeof(ble_temperature_service_t));
	m_temp_service->conn_handle	= BLE_CONN_HANDLE_INVALID;

	// Declare 16-bit service and 128-bit base UUIDs and add them to the BLE stack
	ble_uuid_t				temp_srvc_uuid;
	temp_srvc_uuid.uuid		= BLE_UUID_TEMPERATURE_SERVICE;
	ble_uuid128_t base_uuid = BLE_UUID_MTP_BASE;

	err_code = sd_ble_uuid_vs_add(&base_uuid, &temp_srvc_uuid.type);
	APP_ERROR_CHECK(err_code);    

	err_code = sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY,
		&temp_srvc_uuid,
		&m_temp_service->service_handle);
	APP_ERROR_CHECK(err_code);

	// Add temperature control point characteristic	
	m_temp_service->temperature_ctrl_point.char_uuid.uuid		= BLE_UUID_TEMPERATURE_CTRLPT;
	m_temp_service->temperature_ctrl_point.char_props.write		= true;
	m_temp_service->temperature_ctrl_point.char_props.length	= 3;
	ble_add_characteristic(m_temp_service->service_handle, &m_temp_service->temperature_ctrl_point);
	
	// TODO: NTC temperature (when ADC works well enough)
	
	// Add die temperaure characteristic	
	m_temp_service->die_temperature_char.char_uuid.uuid		= BLE_UUID_TEMPERATURE_DIE_TEMPERATURE;
	m_temp_service->die_temperature_char.char_props.notify	= true;
	m_temp_service->die_temperature_char.char_props.length	= 4;
	ble_add_characteristic(m_temp_service->service_handle, &m_temp_service->die_temperature_char);

	NRF_LOG_INFO("Temperature service service initialized\r\n");
}

void ble_temperature_on_ble_evt(ble_temperature_service_t* m_temp_service, ble_evt_t* p_ble_evt) {
	switch (p_ble_evt->header.evt_id) {
	case BLE_GAP_EVT_CONNECTED:
		m_temp_service->conn_handle	= p_ble_evt->evt.gap_evt.conn_handle;
		break;
	case BLE_GAP_EVT_DISCONNECTED:
		m_temp_service->conn_handle	= BLE_CONN_HANDLE_INVALID;
		break;
	case BLE_GATTS_EVT_WRITE:
		ble_die_temperature_on_write(m_temp_service, p_ble_evt);
		break;
	default:
		// no implementation needed
		break;
	}
}

void temperature_timer_timeout_handler(void * p_context) {
	
	int32_t temperature_quarter_c;
	sd_temp_get(&temperature_quarter_c);
	int16_t temperature_c = temperature_quarter_c * 0.25;
	
	if (temperature_c > 100) {
		uint32_t err_code;
		err_code = sd_power_system_off();
		APP_ERROR_CHECK(err_code);
	}
	
	uint8_t byte_array[4];
	byte_array[0] = (temperature_quarter_c >> 24) & 0xFF;
	byte_array[1] = (temperature_quarter_c >> 16) & 0xFF;
	byte_array[2] = (temperature_quarter_c >> 8) & 0xFF;
	byte_array[3] = (temperature_quarter_c) & 0xFF;

	ble_die_temperature_notify(p_context, byte_array);
}


