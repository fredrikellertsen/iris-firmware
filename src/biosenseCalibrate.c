#include "biosenseCalibrate.h"

/* Module to calibrate the current settings for all LED groups in the system implemented by way of recursive callbacks
 * to do_calibration_iteration(). The reason for the convoluted implementation is 
 *		1.	if biosense_calibrate is called from ble_evt_dispatch context it will run in the same interrupt priority
 *			as the ble event handler. Doing a while(!drdy) would brick the system, hence the recursive 
 *			callback implementation.
 *		2.	if the calibration for some reason takes a lot of time, calls to biosense_calibrate() are not blocking for a
 *			long time. 
 * I apologize to any future firmware developers who might need to debug this mess.
 */

#define INITIAL_CURRENT				HW_LED_CURRENT_MAX / 2
#define INITIAL_DELTA				HW_LED_CURRENT_MAX / 2

// struct to keep track of calibration progress
typedef struct {
	int16_t		delta;
	uint8_t		current_now;
	bool		is_saturated;
	uint8_t		current_LED_group_index;
}calibration_state_t;

static const led_group_t*	LED_groups;							//< pointer to all LED groups
static uint8_t				n_LED_groups;						//< number of LED groups
static bool					is_new_calibration;					//< true when starting a new calibration

static void(*calibration_finished_cb)(bool);					//< callback for finished calibration

/*******************************************************************************************
 * @brief: Reads one sample from the ADS1292 (which is in two's complement) and returns it 
 * in decimal
 * @retval	int32_t	The decimal value of the sample 
 *******************************************************************************************/
static int32_t read_one_sample_in_decimal() {
	uint8_t res[3];
	ads1292_read_sample(res, 1);
	uint32_t data = ((uint32_t)res[0] << 16) + ((uint32_t)res[1] << 8) + res[2];
	return (data & (1 << (24 - 1))) ? data | ~((1 << 24) - 1) : data;
}

/*******************************************************************************************
 * @brief: Performs one iteration of calibration: Reads a sample, calculates current delta
 * to bring the next sample closer to CALIBRATION_TARGET and starts a new sample with callback
 * to itself. If the global variable is_new_calibration is set, the first call will initialize
 * the calibration struct.
 *******************************************************************************************/
static void do_calibration_iteration() {
	
	static calibration_state_t cs;
	
	if (is_new_calibration) {
		// This is the initial calibration iteration, initialize.
		memset(&cs, 0, sizeof(calibration_state_t));
		cs.current_now = INITIAL_CURRENT;
		cs.delta = INITIAL_DELTA;
		is_new_calibration = false;
	} else {
		// Read sample
		int32_t sample = read_one_sample_in_decimal();
		
		// Examine the sample. Calculate deltas to take us closer to target value.
		// Low value is bright light.
		if (sample > CALIBRATION_RANGE_TARGET) {
			cs.is_saturated = true;
			cs.delta = (cs.delta < 0) ? cs.delta / 2 : -cs.delta / 2;
		} else if (sample > CALIBRATION_TARGET) {
			cs.is_saturated = false;
			cs.delta = (cs.delta < 0) ? cs.delta / 2 : -cs.delta / 2;
		} else if (sample > CALIBRATION_RANGE_TARGET * -1) {
			cs.is_saturated = false;
			cs.delta = (cs.delta < 0) ? -cs.delta / 2 : cs.delta / 2;
		} else {
			cs.is_saturated = true;
			cs.delta = (cs.delta < 0) ? -cs.delta / 2 : cs.delta / 2; 
		}
		
		if (cs.delta == 0) {
			// end case for one LED group
			lp55231_set_led_pwm(LED_groups[cs.current_LED_group_index].LED_mask, 0x00);
			if (cs.is_saturated) {
				// we were unable to escape saturation. Reset LED currents.
				lp55231_set_led_current(
					LED_groups[cs.current_LED_group_index].LED_mask, 
					HW_LED_CURRENT_DEFAULT);
				
				// TODO: what next? return? error code to software?
			} else {
				// we are not in saturation. move to next LED group
				cs.current_now = INITIAL_CURRENT;
				cs.delta = INITIAL_DELTA;
				cs.current_LED_group_index++;	
			} 		
		} else {
			// we are not finished with current LED group. update (and clamp) current and proceed
			if ((int16_t)cs.current_now + cs.delta < 0) {
				cs.current_now = 0;
			}else if (cs.current_now + cs.delta > HW_LED_CURRENT_MAX) {
				cs.current_now = HW_LED_CURRENT_MAX;
			}else {
				cs.current_now += cs.delta;	
			}
			
		}
		
	}
	
	// end case for all LED groups
	if (cs.current_LED_group_index == n_LED_groups) {
		lp55231_set_led_pwm(0xFF, 0x00);
		calibration_finished_cb(true);
		return;
	}
	
	// check if current LED group is empty, skip if it is
	if (LED_groups[cs.current_LED_group_index].LED_mask == 0x00) {
		cs.current_LED_group_index++;
	}
	
	// set (new) current, pwm and start new sample
	lp55231_set_led_current(LED_groups[cs.current_LED_group_index].LED_mask, cs.current_now);
	lp55231_set_led_pwm(LED_groups[cs.current_LED_group_index].LED_mask, 0xFF);
	nrf_delay_us(10);
	ads1292_start(do_calibration_iteration, SINGLE_SHOT);	
	
}

void calibrate_LED_group_currents(const led_group_t* m_LED_groups, uint8_t number_of_LED_groups, void(*finished_cb)(bool)) {
	
	if (finished_cb == NULL) {
		NRF_LOG_ERROR("calibrate_LED_group_currents: callback is null\r\n");
		return;
	} else {
		NRF_LOG_INFO("calibrate_LED_group_currents called for %d led groups. starting iterations \r\n", number_of_LED_groups);
	}

	calibration_finished_cb = finished_cb;
	
	lp55231_set_led_pwm(0xFF, 0x00);
	lp55231_set_led_current(0xFF, 0x00);
	
	LED_groups = m_LED_groups;
	n_LED_groups = number_of_LED_groups;
	is_new_calibration = true;
	
	do_calibration_iteration();
}