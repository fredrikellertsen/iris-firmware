#include "icm20648.h"

// twi master instance
//static const nrf_drv_twi_t m_twi_master = NULL;// NRF_DRV_TWI_INSTANCE(ICM20648_TWI_INSTANCE);
//static volatile bool twi_xfer_done = true;
//
///******************************************************************************************
// * @brief: Callback for twi event on ICM20648_TWI_INSTANCE
// ******************************************************************************************/
//static void twi_event_handler(nrf_drv_twi_evt_t const * p_event, void * p_context) {
//	
//	switch (p_event->type) {
//	case NRF_DRV_TWI_EVT_DONE:
//		twi_xfer_done = true;
//		break;
//	case NRF_DRV_TWI_EVT_ADDRESS_NACK:
//		NRF_LOG_RAW_INFO("icm twi evt address nack\n");
//		break;
//	case NRF_DRV_TWI_EVT_DATA_NACK:
//		NRF_LOG_RAW_INFO("icm twi evt data nack\n");
//		twi_xfer_done = true;
//		break;
//	default:
//		NRF_LOG_RAW_INFO("icm twi unknown evt type: %d\n", p_event->type);
//		break;
//	}
//}
//
// /******************************************************************************************
// * @brief: Writes bytes to ICM20648. First byte must be the address of the register to start 
// * writing into. This function is blocking.
// * @param[in] data Pointer to the data
// * @param[in] data Length of the data to be written
// ******************************************************************************************/
//static void icm20648_write(uint8_t* data, uint8_t length) {
//	
//	uint32_t err_code;
//	twi_xfer_done = false;
//	err_code = nrf_drv_twi_tx(&m_twi_master, ICM20648_ADDRESS, data, length, false);
//	if (err_code != NRF_SUCCESS) {
//		NRF_LOG_RAW_INFO("tx with err_code = %#06x\n", err_code);
//	}
//	while (!twi_xfer_done);
//	
//}
//
// /******************************************************************************************
// * @brief: Reads bytes from LP55231
// * @param[in] reg The register to start reading from
// * @param[in] length The number of bytes to read
// * @param[in] res Pointer to array in which to put the bytes
// ******************************************************************************************/
//static void icm20648_read(uint8_t reg, uint8_t length, uint8_t* res) {
//	
//	uint32_t err_code;
//	
//	icm20648_write(&reg, 1);
//	
//	twi_xfer_done = false;
//	err_code = nrf_drv_twi_rx(&m_twi_master, ICM20648_ADDRESS, res, length);
//	if (err_code != NRF_SUCCESS) {
//		NRF_LOG_RAW_INFO("rx with err_code = %#06x\n", err_code);
//	}
//	while (!twi_xfer_done);
//	
//}
//
//
//void icm20648_init(void) {
//	
//	uint32_t err_code;
//	
//	const nrf_drv_twi_config_t twi_icm20648 = { 
//		.scl = PIN_LP55231_SCL,
//		.sda = PIN_LP55231_SDA,
//		.frequency = NRF_TWI_FREQ_100K,
//		.interrupt_priority = APP_IRQ_PRIORITY_HIGH,
//		.clear_bus_init = false,
//	};
//	
//	err_code = nrf_drv_twi_init(&m_twi_master, &twi_icm20648, twi_event_handler, NULL);
//	if (err_code != NRF_SUCCESS) {
//		NRF_LOG_RAW_INFO("icm20648_init with with err_code = %#06x\n", err_code);
//	}
//	nrf_drv_twi_enable(&m_twi_master);
//	
//}
//
//void icm20648_reset() {
//	
//}