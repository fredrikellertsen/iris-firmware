#include "ledService.h"

void ble_leds_service_init(ble_leds_service_t* m_leds_service) {
	uint32_t   err_code;

	memset(m_leds_service, 0, sizeof(ble_leds_service_t));
	m_leds_service->conn_handle	= BLE_CONN_HANDLE_INVALID;

	ble_uuid_t				leds_srvc_uuid;
	leds_srvc_uuid.uuid		= BLE_UUID_LED_SRVC;
	ble_uuid128_t base_uuid = BLE_UUID_MTP_BASE;

	err_code = sd_ble_uuid_vs_add(&base_uuid, &leds_srvc_uuid.type);
	APP_ERROR_CHECK(err_code);    

	err_code = sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY,
		&leds_srvc_uuid,
		&m_leds_service->service_handle);
	APP_ERROR_CHECK(err_code);

	// Add leds control point characteristic	
	m_leds_service->led_ctrl_pt_char.char_uuid.uuid			= BLE_UUID_LED_CTRL_PT;
	m_leds_service->led_ctrl_pt_char.char_props.write		= true;
	m_leds_service->led_ctrl_pt_char.char_props.length		= 10;
	ble_add_characteristic(m_leds_service->service_handle, &m_leds_service->led_ctrl_pt_char);
	
	// Add led group control point characteristic
	m_leds_service->led_group_ctrl_pt_char.char_uuid.uuid		= BLE_UUID_LED_GROUP_CTRL_PT;
	m_leds_service->led_group_ctrl_pt_char.char_props.write		= true;
	m_leds_service->led_group_ctrl_pt_char.char_props.length	= 10;
	ble_add_characteristic(m_leds_service->service_handle, &m_leds_service->led_group_ctrl_pt_char);

	NRF_LOG_INFO("LEDs service initialized\r\n");
	
}

/*******************************************************************************************
 * @brief: Handles writes to the leds service
 *******************************************************************************************/
static void ble_leds_service_on_write(ble_leds_service_t* m_leds_service, ble_evt_t* p_ble_evt) {
	
	uint16_t char_uuid = p_ble_evt->evt.gatts_evt.params.write.uuid.uuid;
	uint8_t* data = p_ble_evt->evt.gatts_evt.params.write.data;
	uint8_t data_len = p_ble_evt->evt.gatts_evt.params.write.len;
	
	if (char_uuid == m_leds_service->led_ctrl_pt_char.char_uuid.uuid) {
		biosense_set_led_param(data, data_len);
	}	
	
	if (char_uuid == m_leds_service->led_group_ctrl_pt_char.char_uuid.uuid) {
		biosense_set_led_group_param(data, data_len);
	}
}

void ble_leds_service_on_ble_evt(ble_leds_service_t* m_leds_service, ble_evt_t* p_ble_evt) {

	switch (p_ble_evt->header.evt_id) {
	case BLE_GAP_EVT_CONNECTED:
		m_leds_service->conn_handle	= p_ble_evt->evt.gap_evt.conn_handle;
		break;
	case BLE_GAP_EVT_DISCONNECTED:
		m_leds_service->conn_handle	= BLE_CONN_HANDLE_INVALID;
		break;
	case BLE_GATTS_EVT_WRITE:
		ble_leds_service_on_write(m_leds_service, p_ble_evt);
		break;
	default:
		// no implementation needed
		break;
	}
}
