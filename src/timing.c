#include "timing.h"

void timer_init(app_timer_id_t id, app_timer_timeout_handler_t timeout_handler) {
	
	APP_TIMER_INIT(APP_TIMER_PRESCALER, APP_TIMER_OP_QUEUE_SIZE, false);
	app_timer_create(
		&id, 
		APP_TIMER_MODE_REPEATED, 
		timeout_handler);
}

void timer_start(app_timer_id_t id, uint32_t timeout_interval_ms, void* p_context) {
	uint32_t ticks = APP_TIMER_TICKS(timeout_interval_ms, APP_TIMER_PRESCALER);
	app_timer_start(id, ticks, p_context);
}

void timer_stop(app_timer_id_t id) {
	app_timer_stop(id);
}