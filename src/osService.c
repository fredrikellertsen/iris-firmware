#include "osService.h"
#include "biosenseHandler.h"

void ble_os_service_init(ble_os_service_t* m_os_service) {
	uint32_t err_code;
	
	memset(m_os_service, 0, sizeof(ble_os_service_t));
	m_os_service->conn_handle = BLE_CONN_HANDLE_INVALID;
	
	// Declare 16-bit service and 128-bit base UUIDs and add them to the BLE stack
	ble_uuid_t				os_srvc_uuid;
	os_srvc_uuid.uuid		= BLE_UUID_OPTICAL_SENSOR_SRVC;
	ble_uuid128_t base_uuid = BLE_UUID_MTP_BASE;
	
	// Add the optical sensor service UUID to the SD
	err_code = sd_ble_uuid_vs_add(&base_uuid, &os_srvc_uuid.type);
	APP_ERROR_CHECK(err_code);
	
	err_code = sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY,
		&os_srvc_uuid,
		&m_os_service->service_handle);	
	APP_ERROR_CHECK(err_code);

	// Add optical sensor control point characteristic
	m_os_service->ctrl_pt_char.char_uuid.uuid		= BLE_UUID_OPTICAL_SENSOR_CTRL_PT;
	m_os_service->ctrl_pt_char.char_props.write		= true;
	m_os_service->ctrl_pt_char.char_props.length	= 10;
	ble_add_characteristic(m_os_service->service_handle, &m_os_service->ctrl_pt_char);
	
	// Add optical sensor samples characteristic
	m_os_service->samples_char.char_uuid.uuid		= BLE_UUID_OPTICAL_SENSOR_SAMPLES;
	m_os_service->samples_char.char_props.notify	= true;
	m_os_service->samples_char.char_props.length	= BIOSESE_DATA_TARGET_SIZE;
	ble_add_characteristic(m_os_service->service_handle, &m_os_service->samples_char);
	
	NRF_LOG_INFO("Optical sensor service initialized\r\n");
}

/*******************************************************************************************
 * @brief: Handles writes to optical sensor service
  *******************************************************************************************/
static void ble_os_service_on_write(ble_os_service_t* m_os_service, ble_gatts_evt_write_t* w_evt) {
	
	// check if write was to the os service control point
	if (w_evt->uuid.uuid == m_os_service->ctrl_pt_char.char_uuid.uuid) {
		biosense_set_os_param(w_evt->data, w_evt->len);
	} 
	
	// check if write was to the optical sensor samples cccd (=> subscription update)
	if (w_evt->handle == m_os_service->samples_char.char_handle.cccd_handle) {
		if (w_evt->data[0] == 1) {
			NRF_LOG_INFO("ble_os_service_on_write: client subscribed\r\n");
			m_os_service->is_client_subscribed = true;
			biosense_start();
		} else {
			NRF_LOG_INFO("ble_os_service_on_write: client unsubscribed\r\n");
			m_os_service->is_client_subscribed = false;
			biosense_stop();
		}
	}
	
}

void ble_os_service_on_ble_evt(ble_os_service_t* m_os_service, ble_evt_t* p_ble_evt) {
	switch (p_ble_evt->header.evt_id) {
	case BLE_GAP_EVT_CONNECTED:
		m_os_service->conn_handle	= p_ble_evt->evt.gap_evt.conn_handle;
		break;
	case BLE_GAP_EVT_DISCONNECTED:
		m_os_service->conn_handle	= BLE_CONN_HANDLE_INVALID;
		biosense_stop();
		break;
	case BLE_GATTS_EVT_WRITE:
		ble_os_service_on_write(m_os_service, &p_ble_evt->evt.gatts_evt.params.write);
		break;
	default:
		// no implementation needed
		break;
	}
}

void ble_os_service_notify(ble_os_service_t* m_os_service, uint8_t* data) {
	
	if (m_os_service->is_client_subscribed == false || m_os_service->conn_handle == BLE_CONN_HANDLE_INVALID) {
		// client is unsubscribed or unconnected, return
		return;
	}
	
	ble_gatts_hvx_params_t	hvx_params;
	memset(&hvx_params, 0, sizeof(hvx_params));

	hvx_params.handle = m_os_service->samples_char.char_handle.value_handle;
	hvx_params.type   = BLE_GATT_HVX_NOTIFICATION;
	hvx_params.p_len  = &m_os_service->samples_char.char_props.length;
	hvx_params.p_data = data;

	uint32_t err_code;
	err_code = sd_ble_gatts_hvx(m_os_service->conn_handle, &hvx_params);	
	if (err_code != NRF_SUCCESS) {
		ble_log_hvx_err_code(err_code);
	}

}