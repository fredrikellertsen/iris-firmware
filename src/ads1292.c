// SKD Includes
#include "nrf_drv_spi.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "app_error.h"
#include "nrf_gpio.h"
#include "nrf_drv_gpiote.h"
#include "nrf_delay.h"

// Project specific includes
#include "board_config.h"
#include "ads1292.h"
#include "systemService.h"

typedef enum {
	INIT_FAIL = 0,
	INIT_SUCCESS
}init_state;

#define ADS1292_MODE_DEFAULT				SINGLE_SHOT	
#define ADS1292_DEFAULT_SAMPLE_FREQUENCY	ADS_SAMPLE_RATE_2000
#define ADS1292_WRITE_SPI_FREQUENCY			SPI_FREQUENCY_FREQUENCY_K250
#define ADS1292_RUNNING_SPI_FREQUENCY		SPI_FREQUENCY_FREQUENCY_M4


// keeps track of the current state of the ADC
typedef struct {
	init_state init;
	bool is_running;		//< true when START is high, which means the ADS1292 is also in RDATAC mode.
	ads1292_modes mode;
	uint8_t gain;			//< bitmask corresponding to current gain
	uint8_t sample_rate;	//< bitmask corresponding to current sample rate
}ads1292_state_t;
static ads1292_state_t m_state;

// SPI control
#define RX_BUF_LEN 72
static uint8_t rx_buf[RX_BUF_LEN * sizeof(uint8_t)];
static const nrf_drv_spi_t spi = NRF_DRV_SPI_INSTANCE(ADS1292_SPI_INSTANCE);

// Callback function for when data is ready to be extracted from the ADS1292 module
static void(*m_data_ready_cb)(void);

/*******************************************************************************************
 * @brief: Sets the SPI frequency.
 * @param[in]	f	The frequency to set. 
 *******************************************************************************************/
static void ads1292_set_spi_frequency(uint32_t f) {
	switch (ADS1292_SPI_INSTANCE) {
	case 0:
		NRF_SPI0->FREQUENCY = f;
		break;
	case 1:
		NRF_SPI1->FREQUENCY = f;
		break;
	default:
		// no implementation needed
		break;
	}
}

/*******************************************************************************************
 * @brief: Sends a command to the ADS1292 module.
 * @param[in]	command		The command to issue
 *******************************************************************************************/
static void ads1292_send_command(uint8_t command) {
	uint32_t err_code;
	err_code = nrf_drv_spi_transfer(&spi, &command, 1, NULL, 0);
	APP_ERROR_CHECK(err_code);
}

/*******************************************************************************************
 * @brief: Writes data into register. Function will default the adc to RDATAC mode after
 * writing.
 * @param[in] reg_adr Address of the register you want to write to
 * @param[in] value Value you want to write
 *******************************************************************************************/
static void ads1292_write_register(uint8_t reg_adr, const uint8_t value) {
	
	uint32_t err_code;
	uint8_t data[] = { (ADSCMD_WREG | reg_adr), 0, value };
	
	ads1292_set_spi_frequency(ADS1292_WRITE_SPI_FREQUENCY);
	
	ads1292_send_command(ADSCMD_SDATAC);
	err_code = nrf_drv_spi_transfer(&spi, data, 3, NULL, 0);
	APP_ERROR_CHECK(err_code);
	ads1292_send_command(ADSCMD_RDATAC);
	
	ads1292_set_spi_frequency(ADS1292_RUNNING_SPI_FREQUENCY);
	
}

/*******************************************************************************************
 * @brief: Reads data from register. Function will default the adc to RDATAC mode after
 * reading.
 * @param[in] reg_adr Address to the register you want to write to
 * @retval unit8_t Contents of the register.
 *******************************************************************************************/
static uint8_t ads1292_read_register(uint8_t reg_adr) {
	
	uint32_t err_code;

	uint8_t res[] = { 0, 0, 0 };
	uint8_t data[] = { (ADSCMD_RREG | reg_adr), 0, 0};

	ads1292_set_spi_frequency(ADS1292_WRITE_SPI_FREQUENCY);
	
	ads1292_send_command(ADSCMD_SDATAC);
	err_code = nrf_drv_spi_transfer(&spi, data, 3, res, 3);
	ads1292_send_command(ADSCMD_RDATAC);
	
	ads1292_set_spi_frequency(ADS1292_RUNNING_SPI_FREQUENCY);
	APP_ERROR_CHECK(err_code);
	return res[2];
}

/*******************************************************************************************
 * @brief: Callback for DRDY pin events. 
 * If m_state.mode is CONTINUOUS:
 *	-	callback to m_data_ready_cb
 * If m_state.mode is SINGLE_SHOT:
 *	-	ads1292_stop is called before m_data_ready_cb
 *******************************************************************************************/
static void drdy_on_evt_cb() {
	if (m_state.mode == SINGLE_SHOT) {
		ads1292_stop();
	}
	m_data_ready_cb();
}

/*******************************************************************************************
 * @brief: Initializes gpio for the ads1292 module.
 *******************************************************************************************/
static void ads1292_gpio_init(void) {
	
	uint32_t err_code;
	
	nrf_gpio_cfg_output(PIN_ADS1292_START);
	nrf_gpio_cfg_output(PIN_ADS1292_RESET);
	nrf_gpio_cfg_input(PIN_ADS1292_DRDY, NRF_GPIO_PIN_NOPULL);
	nrf_gpio_pin_clear(PIN_ADS1292_START);
	nrf_gpio_pin_clear(PIN_ADS1292_RESET);
	
	// initialize GPIOTE for DRDY pin and tie drdy_on_evt_cb to HIGH->LOW events
	err_code = nrf_drv_gpiote_init();
	APP_ERROR_CHECK(err_code);
	
	nrf_drv_gpiote_in_config_t in_config = GPIOTE_CONFIG_IN_SENSE_HITOLO(true);
	in_config.pull = NRF_GPIO_PIN_NOPULL;
	err_code = nrf_drv_gpiote_in_init(
		PIN_ADS1292_DRDY, 
		&in_config, 
		(nrf_drv_gpiote_evt_handler_t)&drdy_on_evt_cb); // TODO: this works, but the event handler should take pin 
														// and polarity as argument... why no crash?
	APP_ERROR_CHECK(err_code);
}

/*******************************************************************************************
 * @brief: Resets ADS1292 module by pulling reset pin low and high again
 *******************************************************************************************/
static void ads1292_reset(void) {
	nrf_gpio_pin_clear(PIN_ADS1292_RESET);
	nrf_delay_us(100);
	nrf_gpio_pin_set(PIN_ADS1292_RESET);
	nrf_delay_us(100);
}

void ads1292_init() {

	NRF_LOG_INFO("ads1292_init start...\r\n");
	
	uint32_t err_code;
	memset(&m_state, 0, sizeof(ads1292_state_t));
	m_state.mode			= ADS1292_MODE_DEFAULT;
	m_state.is_running		= false;
	m_state.init			= INIT_SUCCESS; // we assume successful init until we check contents of ID register
	
	// Configure SPI: SCK active high, sample on trailing edge of clock
	nrf_drv_spi_config_t spi_config = NRF_DRV_SPI_DEFAULT_CONFIG;
	spi_config.ss_pin   = PIN_ADS1292_CS;
	spi_config.miso_pin = PIN_ADS1292_MISO;
	spi_config.mosi_pin = PIN_ADS1292_MOSI;
	spi_config.sck_pin  = PIN_ADS1292_SCLK;
	spi_config.mode		= NRF_DRV_SPI_MODE_1;
	
	err_code = nrf_drv_spi_init(&spi, &spi_config, NULL);
	APP_ERROR_CHECK(err_code);
	
	ads1292_set_spi_frequency(ADS1292_WRITE_SPI_FREQUENCY);
	ads1292_gpio_init();
	
#ifdef PIN_ADS1292_CLKSEL
	nrf_gpio_cfg_output(PIN_ADS1292_CLKSEL);
	nrf_gpio_pin_clear(PIN_ADS1292_CLKSEL);
#endif

	// Set PWDN/RESET high and wait for power-on reset and oscillator start-up 
	nrf_gpio_pin_set(PIN_ADS1292_RESET);
	nrf_delay_ms(1000);
	
	ads1292_reset();
	
#ifdef PIN_ADS1292_CLKSEL
	nrf_gpio_pin_set(PIN_ADS1292_CLKSEL);
#endif
	
	// Read ID register. If zero, we have no communication with ADS1292.
	NRF_LOG_INFO("Reading ID register of ADS1292... ");
	uint8_t id = ads1292_read_register(ADSREG_ID);
	if (!id) {
		NRF_LOG_RAW_INFO(" read of ID register returned zero, assuming ADS1292 broken.\r\n");
		m_state.init = INIT_FAIL;
		ble_system_throw_error(IRIS_ERROR_ADS1292_UNRESPONSIVE, "ID register zero");
		return;
	} else {
		NRF_LOG_RAW_INFO(" success\r\n");
	}
	
	ads1292_set_sample_rate(ADS1292_DEFAULT_SAMPLE_FREQUENCY);
	
	ads1292_write_register(ADSREG_CONFIG2, 0xA0);	// Enable reference buffer
	ads1292_write_register(ADSREG_CH1SET, 0x00);	// Set channel 1 to normal electode input (default)
	ads1292_write_register(ADSREG_CH2SET, 0x00);	// Set channel 1 to normal electode input (default)

	// TODO: why?
	nrf_delay_ms(10);

	ads1292_send_command(ADSCMD_RDATAC);
	ads1292_set_spi_frequency(ADS1292_RUNNING_SPI_FREQUENCY);
}

void ads1292_set_sample_rate(uint8_t rate) {

	if (m_state.init == INIT_FAIL) {
		return;
	}
	
	if (rate > ADS_SAMPLE_RATE_8000) {
		NRF_LOG_ERROR("ads1292_set_sample_rate: invalid sample rate %#06x\r\n");
	} else {
		m_state.sample_rate = rate;
		if (m_state.mode == SINGLE_SHOT) {
			// sample rate bits and single-shot bit are both located in CONFIG1 register
			ads1292_write_register(ADSREG_CONFIG1, (1 << 7) | m_state.sample_rate);	
		} else {
			ads1292_write_register(ADSREG_CONFIG1, m_state.sample_rate);	
		}
	}
	
}

void ads1292_set_pga_gain(uint8_t gain) {
	
	if (m_state.init == INIT_FAIL) {
		return;
	}
	
	switch (gain) {
	case 1:
		m_state.gain = (0b001 << 4);
		break;
	case 2:
		m_state.gain = (0b010 << 4);
		break;
	case 3:
		m_state.gain = (0b011 << 4);
		break;
	case 4:
		m_state.gain = (0b100 << 4);
		break;
	case 6: 
		m_state.gain = (0b000 << 4);
		break;
	case 8:
		m_state.gain = (0b101 << 4);
		break;
	case 12:
		m_state.gain = (0b110 << 4);
		break;
	default: // invalid gain, default to 6
		NRF_LOG_RAW_INFO("ads1292_setPGAGain: invalid gain: %d\r\n, defaulting to 6", gain);
		m_state.gain = (0b000 << 4);
		break;
	}
	
	NRF_LOG_INFO("ads1292_set_pga_gain: setting gain of %d and performing offset calibration\r\n", gain);
	ads1292_write_register(ADSREG_CH1SET, m_state.gain);
	
	// perform offset calibration after gain change as per datasheet
	ads1292_perform_offset_calibration();
	
}

void ads1292_perform_offset_calibration(void) {

	if (m_state.init == INIT_FAIL) {
		return;
	}
	
	// Enable calibration before sending OFFSETCAL command
	ads1292_write_register(ADSREG_RESP2, (1 << 7));
	ads1292_send_command(ADSCMD_OFFSETCAL);
	NRF_LOG_INFO("performed offset calibration\r\n");
}

void ads1292_start(void(*data_ready_cb)(void), ads1292_modes mode) {
	
	if (m_state.init == INIT_FAIL) {
		return;
	}
	
	// handle mode changes
	if (m_state.mode != mode) {
		if (mode == SINGLE_SHOT) {
			// sample rate bits and single-shot bit are both located in CONFIG1 register
			ads1292_write_register(ADSREG_CONFIG1, (1 << 7) | m_state.sample_rate);	
		} else if (mode == CONTINUOUS) {
			ads1292_write_register(ADSREG_CONFIG1, m_state.sample_rate);
		}
		m_state.mode = mode;
	}
	
	// start sample
	m_data_ready_cb		= data_ready_cb;
	m_state.is_running	= true;
	nrf_drv_gpiote_in_event_enable(PIN_ADS1292_DRDY, true);
	nrf_gpio_pin_set(PIN_ADS1292_START);	
	
}

void ads1292_stop(void) {
	
	if (m_state.init == INIT_FAIL) {
		return;
	}
	
	// todo: enter stdby mode.
	if (m_state.is_running) {	
		nrf_drv_gpiote_in_event_disable(PIN_ADS1292_DRDY);
		nrf_gpio_pin_clear(PIN_ADS1292_START);
		m_state.is_running = false;
	}
}

void ads1292_read_sample(uint8_t* dataPtr, uint8_t channel) {
	
	if (m_state.init == INIT_FAIL) {
		return;
	}
	
	static uint32_t err_code;
	static uint8_t data[RX_BUF_LEN];

	// Read out 72 bits (24 status bits + 24 bits � 2 channels)
	err_code = nrf_drv_spi_transfer(&spi, data, RX_BUF_LEN, rx_buf, RX_BUF_LEN);
	APP_ERROR_CHECK(err_code);
	
	// Extract channel data
	if (channel == 1) {
		dataPtr[0] = rx_buf[3];
		dataPtr[1] = rx_buf[4];
		dataPtr[2] = rx_buf[5];
	} else if (channel == 2) {
		dataPtr[0] = rx_buf[6];
		dataPtr[1] = rx_buf[7];
		dataPtr[2] = rx_buf[8];
	} else {
		NRF_LOG_ERROR("ads1292_read_sample: Trying to read non-existent channel %d\r\n", channel);
	}
	
}




	
	/*
	Note on ADS1292 SPI frequencies:
		The SCLK frequency limits for the ADS1292 can be a little confusing when first reading the data sheet. 
		The SCLK limitation for reading back data is different than the SCLK speed limitation when writing to 
		and reading back from the registers. When reading and writing to registers, the maximum SCLK frequency 
		can be 2X the master clock frequency. You can write to the device at slower SCLK frequencies without a 
		problem but the max is master clock set. Reading back data from the converter can be accomplished at 
		speeds as high as 20MHz.
		
	Note on ADS1292 settle time:
		Note that when START is held high and there is a step change in the input signal, it takes 3 tDR for 
		the filter to settle to the new value. Settled data are available on the fourth DRDY pulse.

	Note on ADS1292 offset calibration:
		With RESP2 [7] = 1, you can send the OFFSETCAL command at any time following the SPI timing protocols. 
		When this command is issued, the Data Rate is changed to the lowest available. Then the digital filter 
		is reset, which requires a settling time of 4*tDR. 16 samples will be recorded, averaged and stored 
		on-chip, and this value is automatically subtracted from the PGA output.
		https://e2e.ti.com/support/data_converters/precision_data_converters/f/73/t/369767
	*/
	