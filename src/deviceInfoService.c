#include "deviceInfoService.h"

// Local name variables (for converting from #define to usable variables)
// TODO: Please find a less shitty way to do this.
static uint8_t device_name[]	= DEVICE_NAME;
static uint8_t fw_version[]		= { FW_VERSION };
static uint8_t manufacturer[]	= MANUFACTURER_NAME;
static uint8_t mcu_version[]	= { HW_MCU_VERSION };
static uint8_t acc_version[]	= { HW_ACCELEROMETER_VERSION };
static uint8_t os_version[]		= { HW_OS_VERSION };
static uint8_t device_id[8];

/*******************************************************************************************
 * @brief: Gets the 64 bit system ID from the FICR registers DEVICEID[0] and DEVICEID[1] and
 * chunks it into the ID byte array
  *******************************************************************************************/
static void get_system_id(uint8_t* ID) {
	uint32_t id0 = NRF_FICR->DEVICEID[0];
	uint32_t id1 = NRF_FICR->DEVICEID[1];
	
	*(ID++) = (id0 >> 24) & 0xFF;
	*(ID++) = (id0 >> 16) & 0xFF;
	*(ID++) = (id0 >> 8) & 0xFF;
	*(ID++) = (id0) & 0xFF;
	*(ID++) = (id1 >> 24) & 0xFF;
	*(ID++) = (id1 >> 16) & 0xFF;
	*(ID++) = (id1 >> 8) & 0xFF;
	*(ID++) = (id1) & 0xFF;
}

void ble_device_info_srv_init(ble_device_info_service_t* m_device_info_service) {
	uint32_t   err_code;
	
	memset(m_device_info_service, 0, sizeof(ble_device_info_service_t));
	m_device_info_service->conn_handle	= BLE_CONN_HANDLE_INVALID;

	// Declare 16-bit service and 128-bit base UUIDs and add them to the BLE stack
	ble_uuid_t				device_info_uuid;
	device_info_uuid.uuid	= BLE_UUID_DEVICE_INFO_SRVC;
	ble_uuid128_t base_uuid = BLE_UUID_MTP_BASE;

	err_code = sd_ble_uuid_vs_add(&base_uuid, &device_info_uuid.type);
	APP_ERROR_CHECK(err_code);    

	err_code = sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY,
		&device_info_uuid,
		&m_device_info_service->service_handle);
	APP_ERROR_CHECK(err_code);
	
	// Get the system ID
	get_system_id(device_id);
	
	// Add the name characteristic
	m_device_info_service->name_char.char_uuid.uuid						= BLE_UUID_DEVICE_INFO_NAME;
	m_device_info_service->name_char.char_props.read					= true;
	m_device_info_service->name_char.char_props.length					= 4;
	m_device_info_service->name_char.char_props.init_value				= device_name;
	ble_add_characteristic(m_device_info_service->service_handle, &m_device_info_service->name_char);
	
	// Add the firmware version characteristic
	m_device_info_service->fw_version_char.char_uuid.uuid				= BLE_UUID_DEVICE_INFO_FW_VERSION;
	m_device_info_service->fw_version_char.char_props.read				= true;
	m_device_info_service->fw_version_char.char_props.length			= 1;
	m_device_info_service->fw_version_char.char_props.init_value		= fw_version;
	ble_add_characteristic(m_device_info_service->service_handle, &m_device_info_service->fw_version_char);
	
	// Add the manufacturer characteristic
	m_device_info_service->manufacturer_char.char_uuid.uuid				= BLE_UUID_DEVICE_INFO_MANUFACTURER;
	m_device_info_service->manufacturer_char.char_props.read			= true;
	m_device_info_service->manufacturer_char.char_props.length			= 14;
	m_device_info_service->manufacturer_char.char_props.init_value		= manufacturer;
	ble_add_characteristic(m_device_info_service->service_handle, &m_device_info_service->manufacturer_char);
	
	// Add the MCU version characteristic
	m_device_info_service->mcu_version_char.char_uuid.uuid				= BLE_UUID_DEVICE_INFO_MCU_VERSION;
	m_device_info_service->mcu_version_char.char_props.read				= true;
	m_device_info_service->mcu_version_char.char_props.length			= 1;
	m_device_info_service->mcu_version_char.char_props.init_value		= mcu_version;
	ble_add_characteristic(m_device_info_service->service_handle, &m_device_info_service->mcu_version_char);
	
	// Add the Accelerometer version characteristic
	m_device_info_service->acc_version_char.char_uuid.uuid				= BLE_UUID_DEVICE_INFO_ACCELEROMETER_VERSION;
	m_device_info_service->acc_version_char.char_props.read				= true;
	m_device_info_service->acc_version_char.char_props.length			= 1;
	m_device_info_service->acc_version_char.char_props.init_value		= acc_version;
	ble_add_characteristic(m_device_info_service->service_handle, &m_device_info_service->acc_version_char);
	
	// Add the Optical Sensor version characteristic
	m_device_info_service->os_version_char.char_uuid.uuid				= BLE_UUID_DEVICE_INFO_OPTICAL_SENSOR_VERSION;
	m_device_info_service->os_version_char.char_props.read				= true;
	m_device_info_service->os_version_char.char_props.length			= 1;
	m_device_info_service->os_version_char.char_props.init_value		= os_version;
	ble_add_characteristic(m_device_info_service->service_handle, &m_device_info_service->os_version_char);
	
	// Add the system ID characteristic
	m_device_info_service->device_identifier_char.char_uuid.uuid		= BLE_UUID_DEVICE_INFO_IDENTIFIER;
	m_device_info_service->device_identifier_char.char_props.read		= true;
	m_device_info_service->device_identifier_char.char_props.length		= 8;
	m_device_info_service->device_identifier_char.char_props.init_value	= device_id;
	ble_add_characteristic(m_device_info_service->service_handle, &m_device_info_service->device_identifier_char);
	
	NRF_LOG_INFO("Device info service initialized\r\n");
}
